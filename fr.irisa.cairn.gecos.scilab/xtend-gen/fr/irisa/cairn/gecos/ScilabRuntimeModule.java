/**
 * generated by Xtext 2.12.0
 */
package fr.irisa.cairn.gecos;

import com.google.inject.Binder;
import com.google.inject.name.Names;
import fr.irisa.cairn.gecos.AbstractScilabRuntimeModule;
import fr.irisa.cairn.gecos.ScilabCustomizedLexer;
import fr.irisa.cairn.gecos.ScilabLinkingService;
import fr.irisa.cairn.gecos.valueconversion.ScilabValueConverterService;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.parser.antlr.Lexer;
import org.eclipse.xtext.parser.antlr.LexerBindings;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class ScilabRuntimeModule extends AbstractScilabRuntimeModule {
  @Override
  public Class<? extends ILinkingService> bindILinkingService() {
    return ScilabLinkingService.class;
  }
  
  @Override
  public Class<? extends IValueConverterService> bindIValueConverterService() {
    return ScilabValueConverterService.class;
  }
  
  @Override
  public void configureRuntimeLexer(final Binder binder) {
    binder.<Lexer>bind(Lexer.class).annotatedWith(Names.named(LexerBindings.RUNTIME)).to(ScilabCustomizedLexer.class);
  }
}
