/**
 */
package fr.irisa.cairn.gecos.scilab;

import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.instrs.InstrsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.scilab.ScilabFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' modelDirectory='/fr.irisa.cairn.gecos.scilab/xcore-gen' basePackage='fr.irisa.cairn.gecos'"
 * @generated
 */
public interface ScilabPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scilab";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/scilab";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scilab";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScilabPackage eINSTANCE = fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabProcedureImpl <em>Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabProcedureImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabProcedure()
	 * @generated
	 */
	int SCILAB_PROCEDURE = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__ANNOTATIONS = CorePackage.PROCEDURE_SYMBOL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__TYPE = CorePackage.PROCEDURE_SYMBOL__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__NAME = CorePackage.PROCEDURE_SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__VALUE = CorePackage.PROCEDURE_SYMBOL__VALUE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__SCOPE = CorePackage.PROCEDURE_SYMBOL__SCOPE;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__KIND = CorePackage.PROCEDURE_SYMBOL__KIND;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__PROCEDURE = CorePackage.PROCEDURE_SYMBOL__PROCEDURE;

	/**
	 * The feature id for the '<em><b>Results</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE__RESULTS = CorePackage.PROCEDURE_SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_PROCEDURE_FEATURE_COUNT = CorePackage.PROCEDURE_SYMBOL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabSwitchBlockImpl <em>Switch Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabSwitchBlockImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabSwitchBlock()
	 * @generated
	 */
	int SCILAB_SWITCH_BLOCK = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__ANNOTATIONS = BlocksPackage.BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__PARENT = BlocksPackage.BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__NUMBER = BlocksPackage.BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Dispatch</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__DISPATCH = BlocksPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__CASES = BlocksPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK__DEFAULT = BlocksPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Switch Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_SWITCH_BLOCK_FEATURE_COUNT = BlocksPackage.BLOCK_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabCaseBlockImpl <em>Case Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabCaseBlockImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabCaseBlock()
	 * @generated
	 */
	int SCILAB_CASE_BLOCK = 2;

	/**
	 * The feature id for the '<em><b>Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_CASE_BLOCK__CASE = 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_CASE_BLOCK__BODY = 1;

	/**
	 * The number of structural features of the '<em>Case Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_CASE_BLOCK_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabForBlockImpl <em>For Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabForBlockImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabForBlock()
	 * @generated
	 */
	int SCILAB_FOR_BLOCK = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__ANNOTATIONS = BlocksPackage.FOR_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__PARENT = BlocksPackage.FOR_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__NUMBER = BlocksPackage.FOR_BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Init Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__INIT_BLOCK = BlocksPackage.FOR_BLOCK__INIT_BLOCK;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__TEST_BLOCK = BlocksPackage.FOR_BLOCK__TEST_BLOCK;

	/**
	 * The feature id for the '<em><b>Step Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__STEP_BLOCK = BlocksPackage.FOR_BLOCK__STEP_BLOCK;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__BODY_BLOCK = BlocksPackage.FOR_BLOCK__BODY_BLOCK;

	/**
	 * The feature id for the '<em><b>Iterator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__ITERATOR = BlocksPackage.FOR_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK__RANGE = BlocksPackage.FOR_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>For Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCILAB_FOR_BLOCK_FEATURE_COUNT = BlocksPackage.FOR_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl <em>Control Flow Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getControlFlowStatement()
	 * @generated
	 */
	int CONTROL_FLOW_STATEMENT = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_STATEMENT__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_STATEMENT__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Separators</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_STATEMENT__SEPARATORS = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_STATEMENT__EXPR = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Control Flow Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_STATEMENT_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getExpressionStatement()
	 * @generated
	 */
	int EXPRESSION_STATEMENT = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Separators</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__SEPARATORS = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT__EXPR = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Expression Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_STATEMENT_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.ComplexNumberInstructionImpl <em>Complex Number Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.ComplexNumberInstructionImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getComplexNumberInstruction()
	 * @generated
	 */
	int COMPLEX_NUMBER_INSTRUCTION = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_NUMBER_INSTRUCTION__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_NUMBER_INSTRUCTION__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Im</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_NUMBER_INSTRUCTION__IM = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Complex Number Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_NUMBER_INSTRUCTION_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.gecos.scilab.impl.TypeInfoImpl <em>Type Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.scilab.impl.TypeInfoImpl
	 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getTypeInfo()
	 * @generated
	 */
	int TYPE_INFO = 7;

	/**
	 * The number of structural features of the '<em>Type Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INFO_FEATURE_COUNT = AnnotationsPackage.IANNOTATION_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ScilabProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabProcedure
	 * @generated
	 */
	EClass getScilabProcedure();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.gecos.scilab.ScilabProcedure#getResults <em>Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Results</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabProcedure#getResults()
	 * @see #getScilabProcedure()
	 * @generated
	 */
	EReference getScilabProcedure_Results();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock <em>Switch Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Block</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock
	 * @generated
	 */
	EClass getScilabSwitchBlock();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getDispatch <em>Dispatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dispatch</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getDispatch()
	 * @see #getScilabSwitchBlock()
	 * @generated
	 */
	EReference getScilabSwitchBlock_Dispatch();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getCases <em>Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cases</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getCases()
	 * @see #getScilabSwitchBlock()
	 * @generated
	 */
	EReference getScilabSwitchBlock_Cases();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getDefault <em>Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock#getDefault()
	 * @see #getScilabSwitchBlock()
	 * @generated
	 */
	EReference getScilabSwitchBlock_Default();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ScilabCaseBlock <em>Case Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Block</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabCaseBlock
	 * @generated
	 */
	EClass getScilabCaseBlock();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabCaseBlock#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Case</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabCaseBlock#getCase()
	 * @see #getScilabCaseBlock()
	 * @generated
	 */
	EReference getScilabCaseBlock_Case();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabCaseBlock#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabCaseBlock#getBody()
	 * @see #getScilabCaseBlock()
	 * @generated
	 */
	EReference getScilabCaseBlock_Body();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ScilabForBlock <em>For Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Block</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabForBlock
	 * @generated
	 */
	EClass getScilabForBlock();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabForBlock#getIterator <em>Iterator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iterator</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabForBlock#getIterator()
	 * @see #getScilabForBlock()
	 * @generated
	 */
	EReference getScilabForBlock_Iterator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ScilabForBlock#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabForBlock#getRange()
	 * @see #getScilabForBlock()
	 * @generated
	 */
	EReference getScilabForBlock_Range();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement <em>Control Flow Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Flow Statement</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ControlFlowStatement
	 * @generated
	 */
	EClass getControlFlowStatement();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getSeparators <em>Separators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Separators</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getSeparators()
	 * @see #getControlFlowStatement()
	 * @generated
	 */
	EAttribute getControlFlowStatement_Separators();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expr</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getExpr()
	 * @see #getControlFlowStatement()
	 * @generated
	 */
	EReference getControlFlowStatement_Expr();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ExpressionStatement <em>Expression Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Statement</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ExpressionStatement
	 * @generated
	 */
	EClass getExpressionStatement();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.gecos.scilab.ExpressionStatement#getSeparators <em>Separators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Separators</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ExpressionStatement#getSeparators()
	 * @see #getExpressionStatement()
	 * @generated
	 */
	EAttribute getExpressionStatement_Separators();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.gecos.scilab.ExpressionStatement#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expr</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ExpressionStatement#getExpr()
	 * @see #getExpressionStatement()
	 * @generated
	 */
	EReference getExpressionStatement_Expr();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction <em>Complex Number Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Number Instruction</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction
	 * @generated
	 */
	EClass getComplexNumberInstruction();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction#getIm <em>Im</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Im</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction#getIm()
	 * @see #getComplexNumberInstruction()
	 * @generated
	 */
	EAttribute getComplexNumberInstruction_Im();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.gecos.scilab.TypeInfo <em>Type Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Info</em>'.
	 * @see fr.irisa.cairn.gecos.scilab.TypeInfo
	 * @generated
	 */
	EClass getTypeInfo();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScilabFactory getScilabFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabProcedureImpl <em>Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabProcedureImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabProcedure()
		 * @generated
		 */
		EClass SCILAB_PROCEDURE = eINSTANCE.getScilabProcedure();

		/**
		 * The meta object literal for the '<em><b>Results</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_PROCEDURE__RESULTS = eINSTANCE.getScilabProcedure_Results();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabSwitchBlockImpl <em>Switch Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabSwitchBlockImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabSwitchBlock()
		 * @generated
		 */
		EClass SCILAB_SWITCH_BLOCK = eINSTANCE.getScilabSwitchBlock();

		/**
		 * The meta object literal for the '<em><b>Dispatch</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_SWITCH_BLOCK__DISPATCH = eINSTANCE.getScilabSwitchBlock_Dispatch();

		/**
		 * The meta object literal for the '<em><b>Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_SWITCH_BLOCK__CASES = eINSTANCE.getScilabSwitchBlock_Cases();

		/**
		 * The meta object literal for the '<em><b>Default</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_SWITCH_BLOCK__DEFAULT = eINSTANCE.getScilabSwitchBlock_Default();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabCaseBlockImpl <em>Case Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabCaseBlockImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabCaseBlock()
		 * @generated
		 */
		EClass SCILAB_CASE_BLOCK = eINSTANCE.getScilabCaseBlock();

		/**
		 * The meta object literal for the '<em><b>Case</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_CASE_BLOCK__CASE = eINSTANCE.getScilabCaseBlock_Case();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_CASE_BLOCK__BODY = eINSTANCE.getScilabCaseBlock_Body();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ScilabForBlockImpl <em>For Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabForBlockImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getScilabForBlock()
		 * @generated
		 */
		EClass SCILAB_FOR_BLOCK = eINSTANCE.getScilabForBlock();

		/**
		 * The meta object literal for the '<em><b>Iterator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_FOR_BLOCK__ITERATOR = eINSTANCE.getScilabForBlock_Iterator();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCILAB_FOR_BLOCK__RANGE = eINSTANCE.getScilabForBlock_Range();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl <em>Control Flow Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getControlFlowStatement()
		 * @generated
		 */
		EClass CONTROL_FLOW_STATEMENT = eINSTANCE.getControlFlowStatement();

		/**
		 * The meta object literal for the '<em><b>Separators</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_FLOW_STATEMENT__SEPARATORS = eINSTANCE.getControlFlowStatement_Separators();

		/**
		 * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_FLOW_STATEMENT__EXPR = eINSTANCE.getControlFlowStatement_Expr();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl <em>Expression Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getExpressionStatement()
		 * @generated
		 */
		EClass EXPRESSION_STATEMENT = eINSTANCE.getExpressionStatement();

		/**
		 * The meta object literal for the '<em><b>Separators</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION_STATEMENT__SEPARATORS = eINSTANCE.getExpressionStatement_Separators();

		/**
		 * The meta object literal for the '<em><b>Expr</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION_STATEMENT__EXPR = eINSTANCE.getExpressionStatement_Expr();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.ComplexNumberInstructionImpl <em>Complex Number Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.ComplexNumberInstructionImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getComplexNumberInstruction()
		 * @generated
		 */
		EClass COMPLEX_NUMBER_INSTRUCTION = eINSTANCE.getComplexNumberInstruction();

		/**
		 * The meta object literal for the '<em><b>Im</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEX_NUMBER_INSTRUCTION__IM = eINSTANCE.getComplexNumberInstruction_Im();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.gecos.scilab.impl.TypeInfoImpl <em>Type Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.scilab.impl.TypeInfoImpl
		 * @see fr.irisa.cairn.gecos.scilab.impl.ScilabPackageImpl#getTypeInfo()
		 * @generated
		 */
		EClass TYPE_INFO = eINSTANCE.getTypeInfo();

	}

} //ScilabPackage
