/**
 */
package fr.irisa.cairn.gecos.scilab;

import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Flow Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getSeparators <em>Separators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getControlFlowStatement()
 * @model
 * @generated
 */
public interface ControlFlowStatement extends Instruction {
	/**
	 * Returns the value of the '<em><b>Separators</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Separators</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Separators</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getControlFlowStatement_Separators()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getSeparators();

	/**
	 * Returns the value of the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expr</em>' containment reference.
	 * @see #setExpr(Instruction)
	 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getControlFlowStatement_Expr()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getExpr();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.scilab.ControlFlowStatement#getExpr <em>Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expr</em>' containment reference.
	 * @see #getExpr()
	 * @generated
	 */
	void setExpr(Instruction value);

} // ControlFlowStatement
