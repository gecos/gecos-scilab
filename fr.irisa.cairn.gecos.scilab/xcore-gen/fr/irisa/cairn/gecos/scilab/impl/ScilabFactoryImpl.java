/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScilabFactoryImpl extends EFactoryImpl implements ScilabFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScilabFactory init() {
		try {
			ScilabFactory theScilabFactory = (ScilabFactory)EPackage.Registry.INSTANCE.getEFactory(ScilabPackage.eNS_URI);
			if (theScilabFactory != null) {
				return theScilabFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScilabFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScilabPackage.SCILAB_PROCEDURE: return createScilabProcedure();
			case ScilabPackage.SCILAB_SWITCH_BLOCK: return createScilabSwitchBlock();
			case ScilabPackage.SCILAB_CASE_BLOCK: return createScilabCaseBlock();
			case ScilabPackage.SCILAB_FOR_BLOCK: return createScilabForBlock();
			case ScilabPackage.CONTROL_FLOW_STATEMENT: return createControlFlowStatement();
			case ScilabPackage.EXPRESSION_STATEMENT: return createExpressionStatement();
			case ScilabPackage.COMPLEX_NUMBER_INSTRUCTION: return createComplexNumberInstruction();
			case ScilabPackage.TYPE_INFO: return createTypeInfo();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabProcedure createScilabProcedure() {
		ScilabProcedureImpl scilabProcedure = new ScilabProcedureImpl();
		return scilabProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabSwitchBlock createScilabSwitchBlock() {
		ScilabSwitchBlockImpl scilabSwitchBlock = new ScilabSwitchBlockImpl();
		return scilabSwitchBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabCaseBlock createScilabCaseBlock() {
		ScilabCaseBlockImpl scilabCaseBlock = new ScilabCaseBlockImpl();
		return scilabCaseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabForBlock createScilabForBlock() {
		ScilabForBlockImpl scilabForBlock = new ScilabForBlockImpl();
		return scilabForBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowStatement createControlFlowStatement() {
		ControlFlowStatementImpl controlFlowStatement = new ControlFlowStatementImpl();
		return controlFlowStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionStatement createExpressionStatement() {
		ExpressionStatementImpl expressionStatement = new ExpressionStatementImpl();
		return expressionStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexNumberInstruction createComplexNumberInstruction() {
		ComplexNumberInstructionImpl complexNumberInstruction = new ComplexNumberInstructionImpl();
		return complexNumberInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInfo createTypeInfo() {
		TypeInfoImpl typeInfo = new TypeInfoImpl();
		return typeInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabPackage getScilabPackage() {
		return (ScilabPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScilabPackage getPackage() {
		return ScilabPackage.eINSTANCE;
	}

} //ScilabFactoryImpl
