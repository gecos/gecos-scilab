/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.ControlFlowStatement;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import gecos.instrs.Instruction;
import gecos.instrs.impl.InstructionImpl;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Flow Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl#getSeparators <em>Separators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.impl.ControlFlowStatementImpl#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlFlowStatementImpl extends InstructionImpl implements ControlFlowStatement {
	/**
	 * The cached value of the '{@link #getSeparators() <em>Separators</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeparators()
	 * @generated
	 * @ordered
	 */
	protected EList<String> separators;

	/**
	 * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpr()
	 * @generated
	 * @ordered
	 */
	protected Instruction expr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlFlowStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScilabPackage.Literals.CONTROL_FLOW_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSeparators() {
		if (separators == null) {
			separators = new EDataTypeEList<String>(String.class, this, ScilabPackage.CONTROL_FLOW_STATEMENT__SEPARATORS);
		}
		return separators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getExpr() {
		return expr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpr(Instruction newExpr, NotificationChain msgs) {
		Instruction oldExpr = expr;
		expr = newExpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR, oldExpr, newExpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpr(Instruction newExpr) {
		if (newExpr != expr) {
			NotificationChain msgs = null;
			if (expr != null)
				msgs = ((InternalEObject)expr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR, null, msgs);
			if (newExpr != null)
				msgs = ((InternalEObject)newExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR, null, msgs);
			msgs = basicSetExpr(newExpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR, newExpr, newExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR:
				return basicSetExpr(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScilabPackage.CONTROL_FLOW_STATEMENT__SEPARATORS:
				return getSeparators();
			case ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR:
				return getExpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScilabPackage.CONTROL_FLOW_STATEMENT__SEPARATORS:
				getSeparators().clear();
				getSeparators().addAll((Collection<? extends String>)newValue);
				return;
			case ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR:
				setExpr((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScilabPackage.CONTROL_FLOW_STATEMENT__SEPARATORS:
				getSeparators().clear();
				return;
			case ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR:
				setExpr((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScilabPackage.CONTROL_FLOW_STATEMENT__SEPARATORS:
				return separators != null && !separators.isEmpty();
			case ScilabPackage.CONTROL_FLOW_STATEMENT__EXPR:
				return expr != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (separators: ");
		result.append(separators);
		result.append(')');
		return result.toString();
	}

} //ControlFlowStatementImpl
