/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import gecos.instrs.impl.InstructionImpl;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Number Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.impl.ComplexNumberInstructionImpl#getIm <em>Im</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexNumberInstructionImpl extends InstructionImpl implements ComplexNumberInstruction {
	/**
	 * The default value of the '{@link #getIm() <em>Im</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIm()
	 * @generated
	 * @ordered
	 */
	protected static final double IM_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getIm() <em>Im</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIm()
	 * @generated
	 * @ordered
	 */
	protected double im = IM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexNumberInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScilabPackage.Literals.COMPLEX_NUMBER_INSTRUCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getIm() {
		return im;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIm(double newIm) {
		double oldIm = im;
		im = newIm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScilabPackage.COMPLEX_NUMBER_INSTRUCTION__IM, oldIm, im));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScilabPackage.COMPLEX_NUMBER_INSTRUCTION__IM:
				return getIm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScilabPackage.COMPLEX_NUMBER_INSTRUCTION__IM:
				setIm((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScilabPackage.COMPLEX_NUMBER_INSTRUCTION__IM:
				setIm(IM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScilabPackage.COMPLEX_NUMBER_INSTRUCTION__IM:
				return im != IM_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (im: ");
		result.append(im);
		result.append(')');
		return result.toString();
	}

} //ComplexNumberInstructionImpl
