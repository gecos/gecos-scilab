/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.ExpressionStatement;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import gecos.instrs.Instruction;
import gecos.instrs.impl.InstructionImpl;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl#getSeparators <em>Separators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.impl.ExpressionStatementImpl#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpressionStatementImpl extends InstructionImpl implements ExpressionStatement {
	/**
	 * The cached value of the '{@link #getSeparators() <em>Separators</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeparators()
	 * @generated
	 * @ordered
	 */
	protected EList<String> separators;

	/**
	 * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpr()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> expr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScilabPackage.Literals.EXPRESSION_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSeparators() {
		if (separators == null) {
			separators = new EDataTypeEList<String>(String.class, this, ScilabPackage.EXPRESSION_STATEMENT__SEPARATORS);
		}
		return separators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getExpr() {
		if (expr == null) {
			expr = new EObjectContainmentEList<Instruction>(Instruction.class, this, ScilabPackage.EXPRESSION_STATEMENT__EXPR);
		}
		return expr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScilabPackage.EXPRESSION_STATEMENT__EXPR:
				return ((InternalEList<?>)getExpr()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScilabPackage.EXPRESSION_STATEMENT__SEPARATORS:
				return getSeparators();
			case ScilabPackage.EXPRESSION_STATEMENT__EXPR:
				return getExpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScilabPackage.EXPRESSION_STATEMENT__SEPARATORS:
				getSeparators().clear();
				getSeparators().addAll((Collection<? extends String>)newValue);
				return;
			case ScilabPackage.EXPRESSION_STATEMENT__EXPR:
				getExpr().clear();
				getExpr().addAll((Collection<? extends Instruction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScilabPackage.EXPRESSION_STATEMENT__SEPARATORS:
				getSeparators().clear();
				return;
			case ScilabPackage.EXPRESSION_STATEMENT__EXPR:
				getExpr().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScilabPackage.EXPRESSION_STATEMENT__SEPARATORS:
				return separators != null && !separators.isEmpty();
			case ScilabPackage.EXPRESSION_STATEMENT__EXPR:
				return expr != null && !expr.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (separators: ");
		result.append(separators);
		result.append(')');
		return result.toString();
	}

} //ExpressionStatementImpl
