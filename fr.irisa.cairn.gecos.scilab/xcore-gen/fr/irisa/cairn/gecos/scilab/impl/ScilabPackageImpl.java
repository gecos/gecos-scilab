/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.ComplexNumberInstruction;
import fr.irisa.cairn.gecos.scilab.ControlFlowStatement;
import fr.irisa.cairn.gecos.scilab.ExpressionStatement;
import fr.irisa.cairn.gecos.scilab.ScilabCaseBlock;
import fr.irisa.cairn.gecos.scilab.ScilabFactory;
import fr.irisa.cairn.gecos.scilab.ScilabForBlock;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import fr.irisa.cairn.gecos.scilab.ScilabProcedure;
import fr.irisa.cairn.gecos.scilab.ScilabSwitchBlock;

import fr.irisa.cairn.gecos.scilab.TypeInfo;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScilabPackageImpl extends EPackageImpl implements ScilabPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scilabProcedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scilabSwitchBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scilabCaseBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scilabForBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlFlowStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexNumberInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeInfoEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScilabPackageImpl() {
		super(eNS_URI, ScilabFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ScilabPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScilabPackage init() {
		if (isInited) return (ScilabPackage)EPackage.Registry.INSTANCE.getEPackage(ScilabPackage.eNS_URI);

		// Obtain or create and register package
		ScilabPackageImpl theScilabPackage = (ScilabPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ScilabPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ScilabPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theScilabPackage.createPackageContents();

		// Initialize created meta-data
		theScilabPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theScilabPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScilabPackage.eNS_URI, theScilabPackage);
		return theScilabPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScilabProcedure() {
		return scilabProcedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabProcedure_Results() {
		return (EReference)scilabProcedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScilabSwitchBlock() {
		return scilabSwitchBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabSwitchBlock_Dispatch() {
		return (EReference)scilabSwitchBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabSwitchBlock_Cases() {
		return (EReference)scilabSwitchBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabSwitchBlock_Default() {
		return (EReference)scilabSwitchBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScilabCaseBlock() {
		return scilabCaseBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabCaseBlock_Case() {
		return (EReference)scilabCaseBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabCaseBlock_Body() {
		return (EReference)scilabCaseBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScilabForBlock() {
		return scilabForBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabForBlock_Iterator() {
		return (EReference)scilabForBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScilabForBlock_Range() {
		return (EReference)scilabForBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlFlowStatement() {
		return controlFlowStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getControlFlowStatement_Separators() {
		return (EAttribute)controlFlowStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowStatement_Expr() {
		return (EReference)controlFlowStatementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpressionStatement() {
		return expressionStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpressionStatement_Separators() {
		return (EAttribute)expressionStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExpressionStatement_Expr() {
		return (EReference)expressionStatementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexNumberInstruction() {
		return complexNumberInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplexNumberInstruction_Im() {
		return (EAttribute)complexNumberInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeInfo() {
		return typeInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScilabFactory getScilabFactory() {
		return (ScilabFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scilabProcedureEClass = createEClass(SCILAB_PROCEDURE);
		createEReference(scilabProcedureEClass, SCILAB_PROCEDURE__RESULTS);

		scilabSwitchBlockEClass = createEClass(SCILAB_SWITCH_BLOCK);
		createEReference(scilabSwitchBlockEClass, SCILAB_SWITCH_BLOCK__DISPATCH);
		createEReference(scilabSwitchBlockEClass, SCILAB_SWITCH_BLOCK__CASES);
		createEReference(scilabSwitchBlockEClass, SCILAB_SWITCH_BLOCK__DEFAULT);

		scilabCaseBlockEClass = createEClass(SCILAB_CASE_BLOCK);
		createEReference(scilabCaseBlockEClass, SCILAB_CASE_BLOCK__CASE);
		createEReference(scilabCaseBlockEClass, SCILAB_CASE_BLOCK__BODY);

		scilabForBlockEClass = createEClass(SCILAB_FOR_BLOCK);
		createEReference(scilabForBlockEClass, SCILAB_FOR_BLOCK__ITERATOR);
		createEReference(scilabForBlockEClass, SCILAB_FOR_BLOCK__RANGE);

		controlFlowStatementEClass = createEClass(CONTROL_FLOW_STATEMENT);
		createEAttribute(controlFlowStatementEClass, CONTROL_FLOW_STATEMENT__SEPARATORS);
		createEReference(controlFlowStatementEClass, CONTROL_FLOW_STATEMENT__EXPR);

		expressionStatementEClass = createEClass(EXPRESSION_STATEMENT);
		createEAttribute(expressionStatementEClass, EXPRESSION_STATEMENT__SEPARATORS);
		createEReference(expressionStatementEClass, EXPRESSION_STATEMENT__EXPR);

		complexNumberInstructionEClass = createEClass(COMPLEX_NUMBER_INSTRUCTION);
		createEAttribute(complexNumberInstructionEClass, COMPLEX_NUMBER_INSTRUCTION__IM);

		typeInfoEClass = createEClass(TYPE_INFO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		scilabProcedureEClass.getESuperTypes().add(theCorePackage.getProcedureSymbol());
		scilabSwitchBlockEClass.getESuperTypes().add(theBlocksPackage.getBlock());
		scilabForBlockEClass.getESuperTypes().add(theBlocksPackage.getForBlock());
		controlFlowStatementEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		expressionStatementEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		complexNumberInstructionEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		typeInfoEClass.getESuperTypes().add(theAnnotationsPackage.getIAnnotation());

		// Initialize classes and features; add operations and parameters
		initEClass(scilabProcedureEClass, ScilabProcedure.class, "ScilabProcedure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScilabProcedure_Results(), theCorePackage.getSymbol(), null, "results", null, 0, -1, ScilabProcedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scilabSwitchBlockEClass, ScilabSwitchBlock.class, "ScilabSwitchBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScilabSwitchBlock_Dispatch(), theInstrsPackage.getInstruction(), null, "dispatch", null, 0, 1, ScilabSwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScilabSwitchBlock_Cases(), this.getScilabCaseBlock(), null, "cases", null, 0, -1, ScilabSwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScilabSwitchBlock_Default(), theBlocksPackage.getBlock(), null, "default", null, 0, 1, ScilabSwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scilabCaseBlockEClass, ScilabCaseBlock.class, "ScilabCaseBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScilabCaseBlock_Case(), theInstrsPackage.getInstruction(), null, "case", null, 0, 1, ScilabCaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScilabCaseBlock_Body(), theBlocksPackage.getBlock(), null, "body", null, 0, 1, ScilabCaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scilabForBlockEClass, ScilabForBlock.class, "ScilabForBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScilabForBlock_Iterator(), theInstrsPackage.getInstruction(), null, "iterator", null, 0, 1, ScilabForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScilabForBlock_Range(), theInstrsPackage.getInstruction(), null, "range", null, 0, 1, ScilabForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlFlowStatementEClass, ControlFlowStatement.class, "ControlFlowStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getControlFlowStatement_Separators(), theEcorePackage.getEString(), "separators", null, 0, -1, ControlFlowStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlFlowStatement_Expr(), theInstrsPackage.getInstruction(), null, "expr", null, 0, 1, ControlFlowStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionStatementEClass, ExpressionStatement.class, "ExpressionStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpressionStatement_Separators(), theEcorePackage.getEString(), "separators", null, 0, -1, ExpressionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExpressionStatement_Expr(), theInstrsPackage.getInstruction(), null, "expr", null, 0, -1, ExpressionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexNumberInstructionEClass, ComplexNumberInstruction.class, "ComplexNumberInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplexNumberInstruction_Im(), theEcorePackage.getEDouble(), "im", null, 0, 1, ComplexNumberInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeInfoEClass, TypeInfo.class, "TypeInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //ScilabPackageImpl
