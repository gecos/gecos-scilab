/**
 */
package fr.irisa.cairn.gecos.scilab.impl;

import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import fr.irisa.cairn.gecos.scilab.TypeInfo;
import gecos.annotations.impl.IAnnotationImpl;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TypeInfoImpl extends IAnnotationImpl implements TypeInfo {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScilabPackage.Literals.TYPE_INFO;
	}

} //TypeInfoImpl
