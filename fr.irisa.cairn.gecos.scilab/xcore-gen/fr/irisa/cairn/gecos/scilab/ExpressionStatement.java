/**
 */
package fr.irisa.cairn.gecos.scilab;

import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.ExpressionStatement#getSeparators <em>Separators</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.scilab.ExpressionStatement#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getExpressionStatement()
 * @model
 * @generated
 */
public interface ExpressionStatement extends Instruction {
	/**
	 * Returns the value of the '<em><b>Separators</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Separators</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Separators</em>' attribute list.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getExpressionStatement_Separators()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getSeparators();

	/**
	 * Returns the value of the '<em><b>Expr</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expr</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expr</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getExpressionStatement_Expr()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getExpr();

} // ExpressionStatement
