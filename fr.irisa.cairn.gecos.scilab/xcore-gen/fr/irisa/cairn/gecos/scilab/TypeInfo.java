/**
 */
package fr.irisa.cairn.gecos.scilab;

import gecos.annotations.IAnnotation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.gecos.scilab.ScilabPackage#getTypeInfo()
 * @model
 * @generated
 */
public interface TypeInfo extends IAnnotation {
} // TypeInfo
