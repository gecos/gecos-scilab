/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import gecos.blocks.Block;
import gecos.instrs.Instruction;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock#getCase <em>Case</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabCaseBlock()
 * @model
 * @generated
 */
public interface ScilabCaseBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case</em>' containment reference.
	 * @see #setCase(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabCaseBlock_Case()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getCase();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock#getCase <em>Case</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case</em>' containment reference.
	 * @see #getCase()
	 * @generated
	 */
	void setCase(Instruction value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Block)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabCaseBlock_Body()
	 * @model containment="true"
	 * @generated
	 */
	Block getBody();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Block value);

} // ScilabCaseBlock
