/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import gecos.blocks.ForBlock;
import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabForBlock#getIterator <em>Iterator</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabForBlock#getRange <em>Range</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabForBlock()
 * @model
 * @generated
 */
public interface ScilabForBlock extends ForBlock {
	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' containment reference.
	 * @see #setIterator(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabForBlock_Iterator()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getIterator();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabForBlock#getIterator <em>Iterator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' containment reference.
	 * @see #getIterator()
	 * @generated
	 */
	void setIterator(Instruction value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference.
	 * @see #setRange(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabForBlock_Range()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getRange();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabForBlock#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' containment reference.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(Instruction value);

} // ScilabForBlock
