/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import gecos.blocks.Block;
import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock#getDispatch <em>Dispatch</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock#getCases <em>Cases</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock#getDefault <em>Default</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabSwitchBlock()
 * @model
 * @generated
 */
public interface ScilabSwitchBlock extends Block {
	/**
	 * Returns the value of the '<em><b>Dispatch</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatch</em>' containment reference.
	 * @see #setDispatch(Instruction)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabSwitchBlock_Dispatch()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getDispatch();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock#getDispatch <em>Dispatch</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatch</em>' containment reference.
	 * @see #getDispatch()
	 * @generated
	 */
	void setDispatch(Instruction value);

	/**
	 * Returns the value of the '<em><b>Cases</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cases</em>' containment reference list.
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabSwitchBlock_Cases()
	 * @model containment="true"
	 * @generated
	 */
	EList<ScilabCaseBlock> getCases();

	/**
	 * Returns the value of the '<em><b>Default</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default</em>' containment reference.
	 * @see #setDefault(Block)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabSwitchBlock_Default()
	 * @model containment="true"
	 * @generated
	 */
	Block getDefault();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock#getDefault <em>Default</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default</em>' containment reference.
	 * @see #getDefault()
	 * @generated
	 */
	void setDefault(Block value);

} // ScilabSwitchBlock
