/**
 */
package fr.irisa.cairn.gecos.model.scilab.impl;

import fr.irisa.cairn.gecos.model.scilab.ScilabCaseBlock;
import fr.irisa.cairn.gecos.model.scilab.ScilabPackage;
import fr.irisa.cairn.gecos.model.scilab.ScilabSwitchBlock;
import gecos.blocks.Block;
import gecos.blocks.impl.BlockImpl;
import gecos.instrs.Instruction;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.impl.ScilabSwitchBlockImpl#getDispatch <em>Dispatch</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.impl.ScilabSwitchBlockImpl#getCases <em>Cases</em>}</li>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.impl.ScilabSwitchBlockImpl#getDefault <em>Default</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScilabSwitchBlockImpl extends BlockImpl implements ScilabSwitchBlock {
	/**
	 * The cached value of the '{@link #getDispatch() <em>Dispatch</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatch()
	 * @generated
	 * @ordered
	 */
	protected Instruction dispatch;

	/**
	 * The cached value of the '{@link #getCases() <em>Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCases()
	 * @generated
	 * @ordered
	 */
	protected EList<ScilabCaseBlock> cases;

	/**
	 * The cached value of the '{@link #getDefault() <em>Default</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefault()
	 * @generated
	 * @ordered
	 */
	protected Block default_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScilabSwitchBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScilabPackage.Literals.SCILAB_SWITCH_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getDispatch() {
		return dispatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatch(Instruction newDispatch, NotificationChain msgs) {
		Instruction oldDispatch = dispatch;
		dispatch = newDispatch;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH, oldDispatch, newDispatch);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatch(Instruction newDispatch) {
		if (newDispatch != dispatch) {
			NotificationChain msgs = null;
			if (dispatch != null)
				msgs = ((InternalEObject)dispatch).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH, null, msgs);
			if (newDispatch != null)
				msgs = ((InternalEObject)newDispatch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH, null, msgs);
			msgs = basicSetDispatch(newDispatch, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH, newDispatch, newDispatch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScilabCaseBlock> getCases() {
		if (cases == null) {
			cases = new EObjectContainmentEList<ScilabCaseBlock>(ScilabCaseBlock.class, this, ScilabPackage.SCILAB_SWITCH_BLOCK__CASES);
		}
		return cases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getDefault() {
		return default_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefault(Block newDefault, NotificationChain msgs) {
		Block oldDefault = default_;
		default_ = newDefault;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT, oldDefault, newDefault);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefault(Block newDefault) {
		if (newDefault != default_) {
			NotificationChain msgs = null;
			if (default_ != null)
				msgs = ((InternalEObject)default_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT, null, msgs);
			if (newDefault != null)
				msgs = ((InternalEObject)newDefault).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT, null, msgs);
			msgs = basicSetDefault(newDefault, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT, newDefault, newDefault));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH:
				return basicSetDispatch(null, msgs);
			case ScilabPackage.SCILAB_SWITCH_BLOCK__CASES:
				return ((InternalEList<?>)getCases()).basicRemove(otherEnd, msgs);
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT:
				return basicSetDefault(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH:
				return getDispatch();
			case ScilabPackage.SCILAB_SWITCH_BLOCK__CASES:
				return getCases();
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT:
				return getDefault();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH:
				setDispatch((Instruction)newValue);
				return;
			case ScilabPackage.SCILAB_SWITCH_BLOCK__CASES:
				getCases().clear();
				getCases().addAll((Collection<? extends ScilabCaseBlock>)newValue);
				return;
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT:
				setDefault((Block)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH:
				setDispatch((Instruction)null);
				return;
			case ScilabPackage.SCILAB_SWITCH_BLOCK__CASES:
				getCases().clear();
				return;
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT:
				setDefault((Block)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DISPATCH:
				return dispatch != null;
			case ScilabPackage.SCILAB_SWITCH_BLOCK__CASES:
				return cases != null && !cases.isEmpty();
			case ScilabPackage.SCILAB_SWITCH_BLOCK__DEFAULT:
				return default_ != null;
		}
		return super.eIsSet(featureID);
	}

} //ScilabSwitchBlockImpl
