/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage
 * @generated
 */
public interface ScilabFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScilabFactory eINSTANCE = fr.irisa.cairn.gecos.model.scilab.impl.ScilabFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure</em>'.
	 * @generated
	 */
	ScilabProcedure createScilabProcedure();

	/**
	 * Returns a new object of class '<em>Switch Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Block</em>'.
	 * @generated
	 */
	ScilabSwitchBlock createScilabSwitchBlock();

	/**
	 * Returns a new object of class '<em>Case Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Block</em>'.
	 * @generated
	 */
	ScilabCaseBlock createScilabCaseBlock();

	/**
	 * Returns a new object of class '<em>For Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Block</em>'.
	 * @generated
	 */
	ScilabForBlock createScilabForBlock();

	/**
	 * Returns a new object of class '<em>Control Flow Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Flow Statement</em>'.
	 * @generated
	 */
	ControlFlowStatement createControlFlowStatement();

	/**
	 * Returns a new object of class '<em>Expression Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression Statement</em>'.
	 * @generated
	 */
	ExpressionStatement createExpressionStatement();

	/**
	 * Returns a new object of class '<em>Complex Number Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Number Instruction</em>'.
	 * @generated
	 */
	ComplexNumberInstruction createComplexNumberInstruction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ScilabPackage getScilabPackage();

} //ScilabFactory
