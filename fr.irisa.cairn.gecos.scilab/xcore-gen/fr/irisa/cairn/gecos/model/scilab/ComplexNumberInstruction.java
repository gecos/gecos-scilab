/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Number Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ComplexNumberInstruction#getIm <em>Im</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getComplexNumberInstruction()
 * @model
 * @generated
 */
public interface ComplexNumberInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Im</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Im</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Im</em>' attribute.
	 * @see #setIm(double)
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getComplexNumberInstruction_Im()
	 * @model unique="false"
	 * @generated
	 */
	double getIm();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.gecos.model.scilab.ComplexNumberInstruction#getIm <em>Im</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Im</em>' attribute.
	 * @see #getIm()
	 * @generated
	 */
	void setIm(double value);

} // ComplexNumberInstruction
