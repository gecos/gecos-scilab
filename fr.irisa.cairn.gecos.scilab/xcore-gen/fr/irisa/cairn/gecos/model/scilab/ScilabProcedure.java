/**
 */
package fr.irisa.cairn.gecos.model.scilab;

import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.gecos.model.scilab.ScilabProcedure#getResults <em>Results</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabProcedure()
 * @model
 * @generated
 */
public interface ScilabProcedure extends ProcedureSymbol {
	/**
	 * Returns the value of the '<em><b>Results</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Results</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Results</em>' reference list.
	 * @see fr.irisa.cairn.gecos.model.scilab.ScilabPackage#getScilabProcedure_Results()
	 * @model
	 * @generated
	 */
	EList<Symbol> getResults();

} // ScilabProcedure
