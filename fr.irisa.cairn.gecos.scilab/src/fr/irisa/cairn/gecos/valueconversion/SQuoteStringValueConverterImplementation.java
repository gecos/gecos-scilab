package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class SQuoteStringValueConverterImplementation implements
		IValueConverter<String> {

	
	public String toValue(String string, INode node)
			throws ValueConverterException {
		if(string.length()>=3) {
			return string.substring(1, string.length()-1);
		} else {
			throw new UnsupportedOperationException("Invalid string format "+string);
		}
		}

	
	public String toString(String value) throws ValueConverterException {
		return "'"+value+"'";
	}

}
