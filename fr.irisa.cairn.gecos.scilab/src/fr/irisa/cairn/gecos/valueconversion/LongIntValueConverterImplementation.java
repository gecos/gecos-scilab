package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class LongIntValueConverterImplementation implements
		IValueConverter<Long> {

	
	public Long toValue(String string, INode node) throws ValueConverterException {
		if(string.equals("*")) return -1l; else return Long.parseLong(string);
	}

	public String toString(Long value) throws ValueConverterException {
		if(value<0) return "*"; else return ""+value;
	}

}
