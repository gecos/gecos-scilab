package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;

public class ScilabValueConverterService extends DefaultTerminalConverters {

	@ValueConverter(rule = "FLOATNUMBER")
	public IValueConverter<Double> floatConverter() {
		return new FloatingPointValueConverterImplementation();
	}


	@ValueConverter(rule = "LONGINT") 
	public IValueConverter<Long> longConverter() {
		return new LongIntValueConverterImplementation();
	}

	@ValueConverter(rule = "SQUOTE_STRING")
	public IValueConverter<String> squoteStringConverter() {
		return new SQuoteStringValueConverterImplementation();
	}

	@ValueConverter(rule = "IMAGINARY_FLOATNUMBER")
	public IValueConverter<Double> imaginaryFloatNumberConverter(){
		return new ImaginaryFloatNumberConverterImplementation();
	}

	@ValueConverter(rule = "PRAGMA_ENTRY")
	public IValueConverter<String> pragmaEntryValueConverterImplementation(){
		return new PragmaEntryValueConverterImplementation();
	}

}
