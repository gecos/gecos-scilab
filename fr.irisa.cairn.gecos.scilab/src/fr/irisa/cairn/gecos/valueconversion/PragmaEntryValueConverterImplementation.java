package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class PragmaEntryValueConverterImplementation implements
		IValueConverter<String> {

	
	public String toValue(String string, INode node)
			throws ValueConverterException {
		return string.replace("\n", "");
	}

	
	public String toString(String value) throws ValueConverterException {
		return value+"\n";
	}

}
