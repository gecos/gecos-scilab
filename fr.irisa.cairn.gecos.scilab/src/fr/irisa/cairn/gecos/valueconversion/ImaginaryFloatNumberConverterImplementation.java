package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class ImaginaryFloatNumberConverterImplementation implements
		IValueConverter<Double> {

	public Double toValue(String string, INode node)
			throws ValueConverterException {
		
		return Double.valueOf(string.substring(0,string.length()-1));
		
	}

	public String toString(Double value) throws ValueConverterException {
		return ""+value;
	}

}
