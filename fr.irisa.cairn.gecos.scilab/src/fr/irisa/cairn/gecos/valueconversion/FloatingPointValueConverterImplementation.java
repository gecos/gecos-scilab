package fr.irisa.cairn.gecos.valueconversion;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

public class FloatingPointValueConverterImplementation implements
		IValueConverter<Double> {

	
	public Double toValue(String string, INode node)
			throws ValueConverterException {
		return Double.parseDouble(string);
	}

	
	public String toString(Double value) throws ValueConverterException {
		return ""+value;
	}

}
