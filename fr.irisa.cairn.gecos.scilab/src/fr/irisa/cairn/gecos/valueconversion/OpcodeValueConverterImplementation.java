package fr.irisa.cairn.gecos.valueconversion;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;
// not usd naymore
@Deprecated
public class OpcodeValueConverterImplementation implements
		IValueConverter<String> {

	Map<String,String> map ;

	public OpcodeValueConverterImplementation() {
		map = new HashMap<String,String>();
		map.put("*" , "mul");
		map.put("+" , "add");
		map.put("-" , "sub");
		map.put("/" , "div");
		map.put("%" , "mod");

		map.put("&" , "and");
		map.put("&&" , "land");
		map.put("|" , "or");
		map.put("||" , "lor");

		map.put(":" , "range");
		map.put(".*" , "dotprod");
		map.put("./" , "dotdiv");
		map.put("\\" , "?");
		map.put(".\\" , "?");
		map.put("^" , "pow");
		map.put(".^","dotpow");
		map.put("'" , "transpose");
		map.put(".\'" , "conjugate");
		map.put(">=" , "geq");
		map.put("<=" , "leq");
		map.put("<" , "lt");
		map.put(">" , "gt");
		map.put("~=" , "gt");
		map.put("==" , "eq");
		map.put("~" , "not");
		
		
	}
	
	/*	
	 * ('*" , "mul");map.put("/" , "?");map.put("%" , "?");map.put(".*" , "?");map.put("./" , "?");map.put("\\" , "?");map.put(".\\" , "?");map.put("^" , "?");map.put(".^'  | '\'" , "?");map.put(".\''|'--'|'++" , "?");map.put("!=' |'==' |'<=" , "?");map.put(">=" , "?");map.put("<" , "?");map.put(">" , "?");map.put("!'|'-'|'+'|'--'|'++'|'&&'|'&'|'|'|'||')? 
	 * 
	 */

	
	public String toValue(String string, INode node) throws ValueConverterException {
		return map.get(string);
	}

	
	public String toString(String value) throws ValueConverterException {
		return value;
	}

}
