package fr.irisa.cairn.gecos;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;


import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosprojectPackage;


public class ScilabXtextParser {

	public static ProcedureSet parseString(String content) throws IOException {
		InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
		
		return parseStream(stream);
	}

	public static ProcedureSet parseFile(String filename) throws IOException {
		InputStream in = new FileInputStream(filename);
		return parseStream(in);
	}

	public static ProcedureSet parseStream(InputStream in) throws IOException {
		ProcedureSet ps = parsingAST(in);
		return ps;
	}

	public static ProcedureSet parsingAST(InputStream in) throws IOException {

		ScilabStandaloneSetup instance = new ScilabStandaloneSetup();
		Injector injector = instance.createInjectorAndDoEMFRegistration();

		ScilabStandaloneSetup.doSetup();

		XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);

		IResourceFactory factory = injector.getInstance(IResourceFactory.class);

		XtextResource r = (XtextResource) factory.createResource(URI
				.createURI("internal.test"));

		EPackage.Registry.INSTANCE.put(GecosprojectPackage.eNS_URI,
				GecosprojectPackage.eINSTANCE);

		rs.getResources().add(r);

		r.load(in, null);

		EcoreUtil.resolveAll(r);

		for (org.eclipse.emf.ecore.resource.Resource.Diagnostic error : r
				.getErrors()) {
			System.err.println(error);
			throw new RuntimeException(error.getMessage() + " "
					+ error.getLine());
		}

		r.getParseResult().getRootNode();

		EObject root = r.getParseResult().getRootASTElement();

		return (ProcedureSet) root;
	}
}