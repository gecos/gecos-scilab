package fr.irisa.cairn.gecos;

import java.util.Stack;

import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.MismatchedTokenException;
import org.antlr.runtime.RecognitionException;

import fr.irisa.cairn.gecos.parser.antlr.internal.InternalScilabLexer;
import fr.irisa.cairn.gecos.parser.antlr.internal.InternalScilabParser;

public class ScilabCustomizedLexer extends InternalScilabLexer {

	private static final boolean VERBOSE = false; 

	protected boolean lookAheadMatch(String s) {
		int i = 0;
		while (i < s.length()) {
			int la = input.LA(i + 1);
			int charAt = s.charAt(i);
			
			if (la != charAt) {
				state.failed = true;
				return false;
			}
			i++;
		}
		return true;
	}

	protected boolean lookAheadMatchAt(String s, int offset) {
		int i = 0;
		while (i < s.length()) {
			int la = input.LA(i + 1+offset);
			int charAt = s.charAt(i);
			if (la != charAt) {
				state.failed = true;
				return false;
			}
			i++;
		}
		return true;
	}

	
	protected boolean lookAheadWSMatch(String s) {
		int i = 0;
		

		int k=0;
		boolean ok=true;
		while (ok) {
			int la = input.LA(k + 1);
			System.out.println("k="+k+"->"+la);
			ok=false;
			if(la==' ' || la =='\t') {
				ok=true;
				k++;
			}
		}
		while (i < s.length()) {
			int la = input.LA(k+i + 1);
			int charAt = s.charAt(i);
			System.out.println(la+"="+charAt);
			if (la != charAt) {
				state.failed = true;
				return false;
			}
			i++;
		}
		//System.out.println("Matched "+s);
		return true;
	}
	protected boolean lookBeforeMatch(String s) {
		int i = 0;
		int la = input.LA(i - 1);
		char c = (char) la;
		
		return true;
	}

	private boolean matchId = true;
	protected boolean sQuoteIsTranspose = false;
	protected boolean inPragma= false;
	protected boolean inCDecl= false; 
	
	protected int bracketNesting = 0;
	protected boolean firstToken =true;
	protected Stack<String> stack = new Stack<String>();
	public int prev = -1;
	private boolean newLine =true;
	int cdecl_offset =0;

	@Override
	public void mTokens() throws RecognitionException {
		boolean wsSensitive = false;
		if (stack.size() != 0) {
			String peek = stack.peek();
			if (peek.equals("[")) {
				wsSensitive = true;
			}
		}
		if(firstToken) {
			for(cdecl_offset=1;cdecl_offset<input.size()-4;cdecl_offset++) {
				if(lookAheadMatchAt("//%%", cdecl_offset)) {
					System.out.println("Found CDECL at offset "+cdecl_offset+":\n");
					
					String CDEcl ="$";
					for (int k=1;k<=cdecl_offset;k++) {
						char la =  (char) input.LA(k);
						CDEcl=CDEcl+la;
						
					}
					
					System.out.println("CDECL ="+CDEcl);
					inCDecl=true;
					cdecl_offset-=1;
					break;
				}
			}
		} else {
			inCDecl=false;
		}
		
		if (inCDecl) {
//			for(int i=0;i<cdecl_offset;i++) {
//				input.consume();
//			}
//			this.state.type = RULE_CDECLPRAGMATOKEN;
//            this.state.channel = DEFAULT_TOKEN_CHANNEL;

		} else if (inPragma) {
            matchWS();

                mRULE_CRLF(); 
    			this.state.type = RULE_PRAGMA_ENTRY;
                this.state.channel = DEFAULT_TOKEN_CHANNEL;
                inPragma=false;

//		} else	if (lookAheadMatch("end") && !(lookAheadMatch("endfor")) && !(lookAheadMatch("endwhile")) && !(lookAheadMatch("endif")) && !(lookAheadMatch("endfunction"))) {
//			
//			mRULE_END_KW();
//			if (!stack.isEmpty()) {
//				String peek = stack.peek();
//				if (peek != null && peek.equals("(")) {
//					this.state.type = RULE_LASTINDEX_KW;
//					// Here it should be the vector end keyword
//				}
//			}
		} else if ((lookAheadMatch("//pragma ")) && (newLine||prev==-1)) {
            match("//pragma "); 
            
			this.state.type = RULE_PRAGMA_KW;
            this.state.channel = DEFAULT_TOKEN_CHANNEL;
            inPragma =true;
		} else if ((lookAheadMatch("//EMX:")) && (newLine||prev==-1)) {
            match("//EMX:"); 
            
			this.state.type = RULE_PRAGMA_KW;
            this.state.channel = DEFAULT_TOKEN_CHANNEL;
            inPragma =true;
		} else if (sQuoteIsTranspose && lookAheadMatch("'")) {
            match("'"); 
			this.state.type = RULE_TRANSPOSE_KW;
            this.state.channel = DEFAULT_TOKEN_CHANNEL;
		} 
		
		else if (lookAheadMatch("++")){
			input.consume();
			input.consume();
			if(matchId)
				this.state.type=RULE_POSTINC_KW;
			else
				this.state.type=RULE_PREINC_KW;
			this.state.channel = DEFAULT_TOKEN_CHANNEL;
		}
		
		else if (lookAheadMatch("--")){
			input.consume();
			input.consume();
			if(matchId)
				this.state.type=RULE_POSTDEC_KW;
			else
				this.state.type=RULE_PREDEC_KW;
			this.state.channel = DEFAULT_TOKEN_CHANNEL;
		}
		
		else if (lookAheadMatch("+") || lookAheadMatch("-")) {
            int _channel = DEFAULT_TOKEN_CHANNEL;
            if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                input.consume();
            }  else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }
            state.channel = _channel;
            this.state.type = RULE_ADDSUB_KW;
			if (wsSensitive) {
				switch (prev) {
				case RULE_WS:
				case RULE_LBRACKET:
				case RULE_COMA_KW:
					this.state.type = RULE_UNARY_ADDSUB_KW;
					break;
				default:
					this.state.type = RULE_BINARY_ADDSUB_KW;
					break;
				}
			} 
			
		}
		else if (lookAheadMatch("#")) {
			MismatchedTokenException mse = new MismatchedTokenException('#', input);
            recover(mse);
            throw mse;
		} else {
			super.mTokens();
		}
		firstToken=false;
		switch (this.state.type) {

		case RULE_LPAR: {
			stack.push("(");
			sQuoteIsTranspose = false;
			matchId=false;
			break;
		}

		case RULE_RPAR: {
			if (stack.size() > 0)
				stack.pop();
			sQuoteIsTranspose = true;
			matchId=false;
			break;
		}

		case RULE_LBRACKET: {
			stack.push("[");
			sQuoteIsTranspose = false;
			bracketNesting++;
			matchId=false;
			break;
		}

		case RULE_RBRACKET: {
			if (stack.size() > 0)
				stack.pop();
			sQuoteIsTranspose = true;
			bracketNesting--;
			matchId=false;
			break;
		}

		case RULE_TRANSPOSE_KW: 
		case RULE_ID:
			matchId=true;
			sQuoteIsTranspose = true;
			matchId=true;
			break;
		case RULE_FLOATNUMBER:
		case RULE_LONGINT:
		case RULE_IMAGINARY_FLOATNUMBER:
			sQuoteIsTranspose = true;
			matchId=false;
			break;
		
		case RULE_CRLF:
			inPragma=false;
			newLine=true;
			sQuoteIsTranspose = false;
			break;

		case RULE_WS:
			if(bracketNesting>0 && sQuoteIsTranspose) sQuoteIsTranspose=false;
			break;
			
		default:
			newLine =false;
			sQuoteIsTranspose = false;
			matchId=false;
		}
		if(VERBOSE)
			System.out.println(InternalScilabParser.tokenNames[this.state.type]);
		prev = this.state.type;
	}

	private void matchWS() throws MismatchedSetException {
		loop5:
		    do {
		        int alt5=2;
		        int LA5_0 = input.LA(1);
		        if ( ((LA5_0>='\u0000' && LA5_0<='\t')||(LA5_0>='\u000B' && LA5_0<='\f')||(LA5_0>='\u000E' && LA5_0<='\uFFFF')) ) {
		            alt5=1;
		        }
		        switch (alt5) {
		    	case 1 :
		    	    {
		    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
		    	        input.consume();

		    	    }
		    	    else {
		    	        MismatchedSetException mse = new MismatchedSetException(null,input);
		    	        recover(mse);
		    	        throw mse;}


		    	    }
		    	    break;

		    	default :
		    	    break loop5;
		        }
		    } while (true);
	}
}
