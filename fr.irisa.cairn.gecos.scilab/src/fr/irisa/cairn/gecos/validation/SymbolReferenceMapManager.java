package fr.irisa.cairn.gecos.validation;

import java.util.HashMap;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.IAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.core.Symbol;
import gecos.instrs.SymbolInstruction;

public class SymbolReferenceMapManager {

	HashMap<Integer, SymbolInstruction> symInitMap;
	HashMap<Integer, SymbolInstruction> symTransformMap;
	
	int id=0;
	
	// FIXME
	public HashMap<Symbol, Symbol> getSymMap() {
		return symMap;
	}

	HashMap<Symbol, Symbol> symMap;
	static final String SRCKEY = "ScilabSrc";

	public SymbolReferenceMapManager() {
		symInitMap = new HashMap<Integer, SymbolInstruction>();
		symTransformMap = new HashMap<Integer, SymbolInstruction>();
		symMap = new HashMap<Symbol, Symbol>();
	}
	
	public void bind(SymbolInstruction original,SymbolInstruction copy) {
		//if(!symInitMap.containsKey(original)) {
			original.getAnnotations().put(SRCKEY,GecosUserAnnotationFactory.STRING(""+id));
			symInitMap.put(id, original);
		//}
		//if(!symTransformMap.containsKey(copy)) {
			copy.getAnnotations().put(SRCKEY,GecosUserAnnotationFactory.STRING(""+id));
			symTransformMap.put(id, copy);
		//}  
			id++;
			symMap.put(original.getSymbol(),copy.getSymbol());
	}
	
	public SymbolInstruction getBindingFor(SymbolInstruction e) {
		IAnnotation annotation = e.getAnnotation(SRCKEY);
		if (annotation!=null && (annotation instanceof StringAnnotation)) {
			StringAnnotation str = (StringAnnotation) annotation;
			String content = str.getContent();
			int key = Integer.parseInt(content);
			if(symInitMap.containsKey(key)) {
				SymbolInstruction symbolInstruction = symInitMap.get(key);
				if(e!=symbolInstruction)
					return symbolInstruction;
			}
			if(symTransformMap.containsKey(key))
				return symTransformMap.get(key);
		}
		return null;
	}
	
	public void clear() {
		symInitMap.clear();
		symTransformMap.clear();
		id=0;
	}
}
