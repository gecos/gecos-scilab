package fr.irisa.cairn.gecos;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.nodemodel.INode;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.scilab.ScilabFactory;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import fr.irisa.cairn.gecos.scilab.ScilabForBlock;
import fr.irisa.cairn.gecos.scilab.ScilabProcedure;
import fr.irisa.cairn.gecos.scilab.TypeInfo;
//import fr.irisa.cairn.gecos.scilab.UndefinedType;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.instrs.FieldInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.Field;
import gecos.types.FunctionType;
import gecos.types.RecordType;
import gecos.types.SignModifiers;
import gecos.types.Type;
import gecos.types.TypesFactory;
import gecos.types.UndefinedType;

/**
 * This class implements a custom Linking Service to be able to handle references to symbols 
 * that are not declared statically. Whenever a reference to a Symbol is expected (and not found), 
 * we create a new symbol with the corresponding name and add it to the adequate scope.  
 *  
 * @author steven
 *
 */

public class ScilabLinkingService extends DefaultLinkingService {
	
	private static final boolean VERBOSE = false;

	public Scope getScope(EObject current) {
		while (current!=null) {
			if (current instanceof ProcedureSet) {
				ScopeContainer module = (ScopeContainer) current;
				if(module.getScope()==null) {
					module.setScope(gecos.core.CoreFactory.eINSTANCE.createScope());
				}
				return module.getScope();
			}
			current=current.eContainer();
		}
		return null;
	}

	public Symbol createIntVariable(String name, Scope scope) {
		Symbol sym = gecos.core.CoreFactory.eINSTANCE.createSymbol();
		sym.setName(name);
		scope.getSymbols().add(sym);
		Scope oldscope = GecosUserTypeFactory.getScope();
		GecosUserTypeFactory.setScope(scope);
		BaseType btype= GecosUserTypeFactory.INT(SignModifiers.SIGNED);
		GecosUserTypeFactory.setScope(oldscope);
		sym.setType(btype);
		scope.getTypes().add(btype);
		return sym;
	}
	
	
	public Symbol createVariable(String name, Scope scope) {
		if(name.equals("$")) {
			return createEndSymbol(scope);
		} else {
			Symbol sym = gecos.core.CoreFactory.eINSTANCE.createSymbol();
			sym.setName(name);
			scope.getSymbols().add(sym);
			UndefinedType ftype= TypesFactory.eINSTANCE.createUndefinedType();
			sym.setType(ftype);
			scope.getTypes().add(ftype);
			return sym;			
		}
	}

	public static Symbol createEndSymbol(Scope scope) {
		Symbol sym = gecos.core.CoreFactory.eINSTANCE.createSymbol();
		sym.setName("$");
		scope.getSymbols().add(sym);

		Scope oldscope = GecosUserTypeFactory.getScope();
		GecosUserTypeFactory.setScope(scope);
		BaseType btype= GecosUserTypeFactory.FLOAT();
		GecosUserTypeFactory.setScope(oldscope);
		sym.setType(btype);
		scope.getTypes().add(btype);
		return sym;
	}

	public Field createField(String name, Scope scope) {
		RecordType record = gecos.types.TypesFactory.eINSTANCE.createRecordType();
		Field sym = gecos.types.TypesFactory.eINSTANCE.createField();
		sym.setName(name);
		record.getFields().add(sym);
		scope.getTypes().add(record);
		return sym;
	}
	public Symbol createParameter(String name, Scope scope) {
		Symbol sym = gecos.core.CoreFactory.eINSTANCE.createParameterSymbol();
		sym.setName(name);
		scope.getSymbols().add(sym);
		UndefinedType ftype= TypesFactory.eINSTANCE.createUndefinedType();
		sym.setType(ftype);
		scope.getTypes().add(ftype);
		return sym;
	}

	public ProcedureSymbol createFunction(String name, Scope scope) {
		ProcedureSymbol sym = gecos.core.CoreFactory.eINSTANCE.createProcedureSymbol();
		sym.setName(name);
		scope.getSymbols().add(sym);
		FunctionType ftype= GecosUserTypeFactory.FUNCTION(GecosUserTypeFactory.UNDEFINED());
		sym.setType(ftype);
		scope.getTypes().add(ftype);
		return sym;
	}

	public List<EObject> getLinkedObjects(EObject context, EReference ref, INode node)
			throws IllegalNodeException {
		List<EObject> res = super.getLinkedObjects(context, ref, node);
		
		String text = node.getText();
		if(context instanceof TypeInfo){
			if (VERBOSE) System.err.println("Warning : fixing linking issue for TypeInfo pragma symbol "+text);
			if(res.size()==0) {
				Symbol newSym = createVariable(text, getScope(context));
				return Collections.singletonList((EObject)newSym);
			}
		}
		
		if(context instanceof FieldInstruction){
			if (VERBOSE) System.err.println("Warning : fixing linking issue for Field Info "+text);
			if(res.size()==0) {
				Field newSym = createField(text, getScope(context));
				return Collections.singletonList((EObject)newSym);
			}
		}
		
		if ((context instanceof SymbolInstruction)||(context instanceof ScilabForBlock)) {
			if (VERBOSE) System.err.println("Warning : fixing linking issue for variable symbol "+text);
			if(res.size()==0) {
				Symbol newSym = createVariable(text, getScope(context));
				return Collections.singletonList((EObject)newSym);
			}
		}
		if (context instanceof ScilabProcedure) {
			ScilabProcedure mproc = (ScilabProcedure) context;
			switch(ref.getFeatureID()) {
				//case ScilabPackage.SCILAB_PROCEDURE__ARGS:
				case ScilabPackage.SCILAB_PROCEDURE__RESULTS:
					if(mproc.getScope()==null) {
						mproc.setScope(GecosUserCoreFactory.scope());
					}

					if (VERBOSE) System.err.println("Warning : fixing linking issue for function symbol "+text);
					if(res.size()==0) {
						Symbol newSym = createParameter(text, mproc.getScope());
						return Collections.singletonList((EObject)newSym);
					}
					break;
					
				default:
			}	
		}
		if (context instanceof ScilabForBlock){
			switch(ref.getFeatureID()) {
				case ScilabPackage.SCILAB_FOR_BLOCK__ITERATOR:
					if (VERBOSE) System.err.println("Warning : fixing linking issue for iterator symbol "+text);
						Symbol newSym = createIntVariable(text,getScope(context)); 
						return Collections.singletonList((EObject)newSym);

				default :
			}
		}
		return res;
	}
}