/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.cairn.gecos.ide

import com.google.inject.Guice
import fr.irisa.cairn.gecos.ScilabRuntimeModule
import fr.irisa.cairn.gecos.ScilabStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class ScilabIdeSetup extends ScilabStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new ScilabRuntimeModule, new ScilabIdeModule))
	}
	
}
