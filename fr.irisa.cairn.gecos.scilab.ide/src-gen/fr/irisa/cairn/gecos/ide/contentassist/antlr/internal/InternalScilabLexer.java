package fr.irisa.cairn.gecos.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalScilabLexer extends Lexer {
    public static final int RULE_FLOATNUMBER=62;
    public static final int RULE_SQUOTE_STRING=27;
    public static final int RULE_FUNCTION_KW=34;
    public static final int RULE_OR_KW=58;
    public static final int RULE_POSTDEC_KW=31;
    public static final int RULE_ARITH_KW=60;
    public static final int RULE_AT_KW=55;
    public static final int RULE_WHILE_KW=46;
    public static final int RULE_CONJUGATE_KW=25;
    public static final int RULE_LT_KW=14;
    public static final int RULE_CASE_KW=45;
    public static final int RULE_PREDEC_KW=29;
    public static final int RULE_OTHERWISE_KW=44;
    public static final int RULE_IF_KW=39;
    public static final int RULE_ID=52;
    public static final int RULE_GE_KW=17;
    public static final int RULE_LAND_KW=57;
    public static final int RULE_GT_KW=15;
    public static final int RULE_LONGINT=61;
    public static final int RULE_NOT_KW=23;
    public static final int RULE_END_KW=6;
    public static final int RULE_CRLF=5;
    public static final int RULE_ELSEIF_KW=42;
    public static final int RULE_CONTINUE_KW=49;
    public static final int RULE_LPAR=32;
    public static final int RULE_LBRACKET=36;
    public static final int T__70=70;
    public static final int RULE_BOOLEAN_VALUE=65;
    public static final int T__71=71;
    public static final int RULE_DOT_KW=51;
    public static final int RULE_ENDIF_KW=8;
    public static final int RULE_RESERVED=64;
    public static final int RULE_SL_COMMENT=66;
    public static final int RULE_ENDWHILE_KW=10;
    public static final int RULE_RETURN_KW=47;
    public static final int RULE_NEQ_KW=19;
    public static final int RULE_TRANSPOSE_KW=24;
    public static final int RULE_COMA_KW=4;
    public static final int RULE_LOR_KW=56;
    public static final int RULE_ENDFUNCTION_KW=7;
    public static final int RULE_ELLIPSIS=68;
    public static final int RULE_ADDSUB_KW=20;
    public static final int EOF=-1;
    public static final int RULE_LASTINDEX_KW=38;
    public static final int RULE_SEMICOLON_KW=9;
    public static final int RULE_SWITCH_KW=43;
    public static final int RULE_BREAK_KW=48;
    public static final int RULE_WS=67;
    public static final int RULE_PRAGMA_KW=53;
    public static final int RULE_EQ_KW=18;
    public static final int RULE_ANY_OTHER=69;
    public static final int RULE_FOR_KW=11;
    public static final int RULE_ENDFOR_KW=13;
    public static final int RULE_ASSIGN_KW=35;
    public static final int RULE_PARFOR_KW=12;
    public static final int RULE_DQUOTE_STRING=26;
    public static final int RULE_IMAGINARY_FLOATNUMBER=63;
    public static final int RULE_BINARY_ADDSUB_KW=21;
    public static final int RULE_THEN_KW=40;
    public static final int RULE_PREINC_KW=28;
    public static final int RULE_PRAGMA_ENTRY=54;
    public static final int RULE_ELSE_KW=41;
    public static final int RULE_RPAR=33;
    public static final int RULE_AND_KW=59;
    public static final int RULE_COLON_KW=50;
    public static final int RULE_LE_KW=16;
    public static final int RULE_POSTINC_KW=30;
    public static final int RULE_UNARY_ADDSUB_KW=22;
    public static final int RULE_RBRACKET=37;

    // delegates
    // delegators

    public InternalScilabLexer() {;} 
    public InternalScilabLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalScilabLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalScilab.g"; }

    // $ANTLR start "T__70"
    public final void mT__70() throws RecognitionException {
        try {
            int _type = T__70;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11:7: ( 'global' )
            // InternalScilab.g:11:9: 'global'
            {
            match("global"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__70"

    // $ANTLR start "T__71"
    public final void mT__71() throws RecognitionException {
        try {
            int _type = T__71;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:12:7: ( 'persistent' )
            // InternalScilab.g:12:9: 'persistent'
            {
            match("persistent"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__71"

    // $ANTLR start "RULE_ASSIGN_KW"
    public final void mRULE_ASSIGN_KW() throws RecognitionException {
        try {
            int _type = RULE_ASSIGN_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11522:16: ( '=' )
            // InternalScilab.g:11522:18: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ASSIGN_KW"

    // $ANTLR start "RULE_LOR_KW"
    public final void mRULE_LOR_KW() throws RecognitionException {
        try {
            int _type = RULE_LOR_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11524:13: ( '||' )
            // InternalScilab.g:11524:15: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LOR_KW"

    // $ANTLR start "RULE_LAND_KW"
    public final void mRULE_LAND_KW() throws RecognitionException {
        try {
            int _type = RULE_LAND_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11526:14: ( '&&' )
            // InternalScilab.g:11526:16: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LAND_KW"

    // $ANTLR start "RULE_OR_KW"
    public final void mRULE_OR_KW() throws RecognitionException {
        try {
            int _type = RULE_OR_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11528:12: ( '|' )
            // InternalScilab.g:11528:14: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OR_KW"

    // $ANTLR start "RULE_AND_KW"
    public final void mRULE_AND_KW() throws RecognitionException {
        try {
            int _type = RULE_AND_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11530:13: ( '&' )
            // InternalScilab.g:11530:15: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AND_KW"

    // $ANTLR start "RULE_LT_KW"
    public final void mRULE_LT_KW() throws RecognitionException {
        try {
            int _type = RULE_LT_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11532:12: ( '<' )
            // InternalScilab.g:11532:14: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LT_KW"

    // $ANTLR start "RULE_GT_KW"
    public final void mRULE_GT_KW() throws RecognitionException {
        try {
            int _type = RULE_GT_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11534:12: ( '>' )
            // InternalScilab.g:11534:14: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GT_KW"

    // $ANTLR start "RULE_LE_KW"
    public final void mRULE_LE_KW() throws RecognitionException {
        try {
            int _type = RULE_LE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11536:12: ( '<=' )
            // InternalScilab.g:11536:14: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LE_KW"

    // $ANTLR start "RULE_GE_KW"
    public final void mRULE_GE_KW() throws RecognitionException {
        try {
            int _type = RULE_GE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11538:12: ( '>=' )
            // InternalScilab.g:11538:14: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GE_KW"

    // $ANTLR start "RULE_EQ_KW"
    public final void mRULE_EQ_KW() throws RecognitionException {
        try {
            int _type = RULE_EQ_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11540:12: ( '==' )
            // InternalScilab.g:11540:14: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EQ_KW"

    // $ANTLR start "RULE_NEQ_KW"
    public final void mRULE_NEQ_KW() throws RecognitionException {
        try {
            int _type = RULE_NEQ_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11542:13: ( '/=' )
            // InternalScilab.g:11542:15: '/='
            {
            match("/="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NEQ_KW"

    // $ANTLR start "RULE_NOT_KW"
    public final void mRULE_NOT_KW() throws RecognitionException {
        try {
            int _type = RULE_NOT_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11544:13: ( '~' )
            // InternalScilab.g:11544:15: '~'
            {
            match('~'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NOT_KW"

    // $ANTLR start "RULE_AT_KW"
    public final void mRULE_AT_KW() throws RecognitionException {
        try {
            int _type = RULE_AT_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11546:12: ( '@' )
            // InternalScilab.g:11546:14: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AT_KW"

    // $ANTLR start "RULE_COLON_KW"
    public final void mRULE_COLON_KW() throws RecognitionException {
        try {
            int _type = RULE_COLON_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11548:15: ( ':' )
            // InternalScilab.g:11548:17: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLON_KW"

    // $ANTLR start "RULE_ARITH_KW"
    public final void mRULE_ARITH_KW() throws RecognitionException {
        try {
            int _type = RULE_ARITH_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11550:15: ( ( '*' | '/' | '//' | '.*' | './' | '\\\\' | '.\\\\' | '**' | '.**' | '.^' ) )
            // InternalScilab.g:11550:17: ( '*' | '/' | '//' | '.*' | './' | '\\\\' | '.\\\\' | '**' | '.**' | '.^' )
            {
            // InternalScilab.g:11550:17: ( '*' | '/' | '//' | '.*' | './' | '\\\\' | '.\\\\' | '**' | '.**' | '.^' )
            int alt1=10;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // InternalScilab.g:11550:18: '*'
                    {
                    match('*'); 

                    }
                    break;
                case 2 :
                    // InternalScilab.g:11550:22: '/'
                    {
                    match('/'); 

                    }
                    break;
                case 3 :
                    // InternalScilab.g:11550:26: '//'
                    {
                    match("//"); 


                    }
                    break;
                case 4 :
                    // InternalScilab.g:11550:31: '.*'
                    {
                    match(".*"); 


                    }
                    break;
                case 5 :
                    // InternalScilab.g:11550:36: './'
                    {
                    match("./"); 


                    }
                    break;
                case 6 :
                    // InternalScilab.g:11550:41: '\\\\'
                    {
                    match('\\'); 

                    }
                    break;
                case 7 :
                    // InternalScilab.g:11550:46: '.\\\\'
                    {
                    match(".\\"); 


                    }
                    break;
                case 8 :
                    // InternalScilab.g:11550:52: '**'
                    {
                    match("**"); 


                    }
                    break;
                case 9 :
                    // InternalScilab.g:11550:57: '.**'
                    {
                    match(".**"); 


                    }
                    break;
                case 10 :
                    // InternalScilab.g:11550:63: '.^'
                    {
                    match(".^"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ARITH_KW"

    // $ANTLR start "RULE_ADDSUB_KW"
    public final void mRULE_ADDSUB_KW() throws RecognitionException {
        try {
            int _type = RULE_ADDSUB_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11552:16: ( ( '#A+' | '#A-' ) )
            // InternalScilab.g:11552:18: ( '#A+' | '#A-' )
            {
            // InternalScilab.g:11552:18: ( '#A+' | '#A-' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='#') ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='A') ) {
                    int LA2_2 = input.LA(3);

                    if ( (LA2_2=='+') ) {
                        alt2=1;
                    }
                    else if ( (LA2_2=='-') ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalScilab.g:11552:19: '#A+'
                    {
                    match("#A+"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11552:25: '#A-'
                    {
                    match("#A-"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ADDSUB_KW"

    // $ANTLR start "RULE_BINARY_ADDSUB_KW"
    public final void mRULE_BINARY_ADDSUB_KW() throws RecognitionException {
        try {
            int _type = RULE_BINARY_ADDSUB_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11554:23: ( ( '#B-' | '#B+' ) )
            // InternalScilab.g:11554:25: ( '#B-' | '#B+' )
            {
            // InternalScilab.g:11554:25: ( '#B-' | '#B+' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='#') ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1=='B') ) {
                    int LA3_2 = input.LA(3);

                    if ( (LA3_2=='-') ) {
                        alt3=1;
                    }
                    else if ( (LA3_2=='+') ) {
                        alt3=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalScilab.g:11554:26: '#B-'
                    {
                    match("#B-"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11554:32: '#B+'
                    {
                    match("#B+"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARY_ADDSUB_KW"

    // $ANTLR start "RULE_UNARY_ADDSUB_KW"
    public final void mRULE_UNARY_ADDSUB_KW() throws RecognitionException {
        try {
            int _type = RULE_UNARY_ADDSUB_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11556:22: ( ( '#U-' | '#U+' ) )
            // InternalScilab.g:11556:24: ( '#U-' | '#U+' )
            {
            // InternalScilab.g:11556:24: ( '#U-' | '#U+' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='#') ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1=='U') ) {
                    int LA4_2 = input.LA(3);

                    if ( (LA4_2=='-') ) {
                        alt4=1;
                    }
                    else if ( (LA4_2=='+') ) {
                        alt4=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalScilab.g:11556:25: '#U-'
                    {
                    match("#U-"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11556:31: '#U+'
                    {
                    match("#U+"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNARY_ADDSUB_KW"

    // $ANTLR start "RULE_TRANSPOSE_KW"
    public final void mRULE_TRANSPOSE_KW() throws RecognitionException {
        try {
            int _type = RULE_TRANSPOSE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11558:19: ( '#\\'' )
            // InternalScilab.g:11558:21: '#\\''
            {
            match("#'"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TRANSPOSE_KW"

    // $ANTLR start "RULE_PREINC_KW"
    public final void mRULE_PREINC_KW() throws RecognitionException {
        try {
            int _type = RULE_PREINC_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11560:16: ( 'pre++' )
            // InternalScilab.g:11560:18: 'pre++'
            {
            match("pre++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PREINC_KW"

    // $ANTLR start "RULE_PREDEC_KW"
    public final void mRULE_PREDEC_KW() throws RecognitionException {
        try {
            int _type = RULE_PREDEC_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11562:16: ( 'pre--' )
            // InternalScilab.g:11562:18: 'pre--'
            {
            match("pre--"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PREDEC_KW"

    // $ANTLR start "RULE_POSTINC_KW"
    public final void mRULE_POSTINC_KW() throws RecognitionException {
        try {
            int _type = RULE_POSTINC_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11564:17: ( 'post++' )
            // InternalScilab.g:11564:19: 'post++'
            {
            match("post++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_POSTINC_KW"

    // $ANTLR start "RULE_POSTDEC_KW"
    public final void mRULE_POSTDEC_KW() throws RecognitionException {
        try {
            int _type = RULE_POSTDEC_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11566:17: ( 'post--' )
            // InternalScilab.g:11566:19: 'post--'
            {
            match("post--"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_POSTDEC_KW"

    // $ANTLR start "RULE_CONJUGATE_KW"
    public final void mRULE_CONJUGATE_KW() throws RecognitionException {
        try {
            int _type = RULE_CONJUGATE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11568:19: ( '.\\'' )
            // InternalScilab.g:11568:21: '.\\''
            {
            match(".'"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONJUGATE_KW"

    // $ANTLR start "RULE_LPAR"
    public final void mRULE_LPAR() throws RecognitionException {
        try {
            int _type = RULE_LPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11570:11: ( '(' )
            // InternalScilab.g:11570:13: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LPAR"

    // $ANTLR start "RULE_RPAR"
    public final void mRULE_RPAR() throws RecognitionException {
        try {
            int _type = RULE_RPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11572:11: ( ')' )
            // InternalScilab.g:11572:13: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RPAR"

    // $ANTLR start "RULE_LBRACKET"
    public final void mRULE_LBRACKET() throws RecognitionException {
        try {
            int _type = RULE_LBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11574:15: ( '[' )
            // InternalScilab.g:11574:17: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LBRACKET"

    // $ANTLR start "RULE_RBRACKET"
    public final void mRULE_RBRACKET() throws RecognitionException {
        try {
            int _type = RULE_RBRACKET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11576:15: ( ']' )
            // InternalScilab.g:11576:17: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RBRACKET"

    // $ANTLR start "RULE_SEMICOLON_KW"
    public final void mRULE_SEMICOLON_KW() throws RecognitionException {
        try {
            int _type = RULE_SEMICOLON_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11578:19: ( ';' )
            // InternalScilab.g:11578:21: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEMICOLON_KW"

    // $ANTLR start "RULE_COMA_KW"
    public final void mRULE_COMA_KW() throws RecognitionException {
        try {
            int _type = RULE_COMA_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11580:14: ( ',' )
            // InternalScilab.g:11580:16: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMA_KW"

    // $ANTLR start "RULE_RETURN_KW"
    public final void mRULE_RETURN_KW() throws RecognitionException {
        try {
            int _type = RULE_RETURN_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11582:16: ( 'return' )
            // InternalScilab.g:11582:18: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RETURN_KW"

    // $ANTLR start "RULE_FUNCTION_KW"
    public final void mRULE_FUNCTION_KW() throws RecognitionException {
        try {
            int _type = RULE_FUNCTION_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11584:18: ( 'function' )
            // InternalScilab.g:11584:20: 'function'
            {
            match("function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FUNCTION_KW"

    // $ANTLR start "RULE_ENDFUNCTION_KW"
    public final void mRULE_ENDFUNCTION_KW() throws RecognitionException {
        try {
            int _type = RULE_ENDFUNCTION_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11586:21: ( 'endfunction' )
            // InternalScilab.g:11586:23: 'endfunction'
            {
            match("endfunction"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENDFUNCTION_KW"

    // $ANTLR start "RULE_ENDFOR_KW"
    public final void mRULE_ENDFOR_KW() throws RecognitionException {
        try {
            int _type = RULE_ENDFOR_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11588:16: ( 'endfor' )
            // InternalScilab.g:11588:18: 'endfor'
            {
            match("endfor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENDFOR_KW"

    // $ANTLR start "RULE_ENDIF_KW"
    public final void mRULE_ENDIF_KW() throws RecognitionException {
        try {
            int _type = RULE_ENDIF_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11590:15: ( 'endif' )
            // InternalScilab.g:11590:17: 'endif'
            {
            match("endif"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENDIF_KW"

    // $ANTLR start "RULE_ENDWHILE_KW"
    public final void mRULE_ENDWHILE_KW() throws RecognitionException {
        try {
            int _type = RULE_ENDWHILE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11592:18: ( 'endwhile' )
            // InternalScilab.g:11592:20: 'endwhile'
            {
            match("endwhile"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ENDWHILE_KW"

    // $ANTLR start "RULE_WHILE_KW"
    public final void mRULE_WHILE_KW() throws RecognitionException {
        try {
            int _type = RULE_WHILE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11594:15: ( 'while' )
            // InternalScilab.g:11594:17: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WHILE_KW"

    // $ANTLR start "RULE_ELSE_KW"
    public final void mRULE_ELSE_KW() throws RecognitionException {
        try {
            int _type = RULE_ELSE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11596:14: ( 'else' )
            // InternalScilab.g:11596:16: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELSE_KW"

    // $ANTLR start "RULE_THEN_KW"
    public final void mRULE_THEN_KW() throws RecognitionException {
        try {
            int _type = RULE_THEN_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11598:14: ( 'then' )
            // InternalScilab.g:11598:16: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_THEN_KW"

    // $ANTLR start "RULE_ELSEIF_KW"
    public final void mRULE_ELSEIF_KW() throws RecognitionException {
        try {
            int _type = RULE_ELSEIF_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11600:16: ( 'elseif' )
            // InternalScilab.g:11600:18: 'elseif'
            {
            match("elseif"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELSEIF_KW"

    // $ANTLR start "RULE_PARFOR_KW"
    public final void mRULE_PARFOR_KW() throws RecognitionException {
        try {
            int _type = RULE_PARFOR_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11602:16: ( 'forall' )
            // InternalScilab.g:11602:18: 'forall'
            {
            match("forall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PARFOR_KW"

    // $ANTLR start "RULE_FOR_KW"
    public final void mRULE_FOR_KW() throws RecognitionException {
        try {
            int _type = RULE_FOR_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11604:13: ( 'for' )
            // InternalScilab.g:11604:15: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FOR_KW"

    // $ANTLR start "RULE_IF_KW"
    public final void mRULE_IF_KW() throws RecognitionException {
        try {
            int _type = RULE_IF_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11606:12: ( 'if' )
            // InternalScilab.g:11606:14: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IF_KW"

    // $ANTLR start "RULE_END_KW"
    public final void mRULE_END_KW() throws RecognitionException {
        try {
            int _type = RULE_END_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11608:13: ( 'end' )
            // InternalScilab.g:11608:15: 'end'
            {
            match("end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_END_KW"

    // $ANTLR start "RULE_LASTINDEX_KW"
    public final void mRULE_LASTINDEX_KW() throws RecognitionException {
        try {
            int _type = RULE_LASTINDEX_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11610:19: ( '$' )
            // InternalScilab.g:11610:21: '$'
            {
            match('$'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LASTINDEX_KW"

    // $ANTLR start "RULE_BREAK_KW"
    public final void mRULE_BREAK_KW() throws RecognitionException {
        try {
            int _type = RULE_BREAK_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11612:15: ( 'break' )
            // InternalScilab.g:11612:17: 'break'
            {
            match("break"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BREAK_KW"

    // $ANTLR start "RULE_CONTINUE_KW"
    public final void mRULE_CONTINUE_KW() throws RecognitionException {
        try {
            int _type = RULE_CONTINUE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11614:18: ( 'continue' )
            // InternalScilab.g:11614:20: 'continue'
            {
            match("continue"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONTINUE_KW"

    // $ANTLR start "RULE_SWITCH_KW"
    public final void mRULE_SWITCH_KW() throws RecognitionException {
        try {
            int _type = RULE_SWITCH_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11616:16: ( 'switch' )
            // InternalScilab.g:11616:18: 'switch'
            {
            match("switch"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SWITCH_KW"

    // $ANTLR start "RULE_OTHERWISE_KW"
    public final void mRULE_OTHERWISE_KW() throws RecognitionException {
        try {
            int _type = RULE_OTHERWISE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11618:19: ( 'otherwise' )
            // InternalScilab.g:11618:21: 'otherwise'
            {
            match("otherwise"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OTHERWISE_KW"

    // $ANTLR start "RULE_CASE_KW"
    public final void mRULE_CASE_KW() throws RecognitionException {
        try {
            int _type = RULE_CASE_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11620:14: ( 'case' )
            // InternalScilab.g:11620:16: 'case'
            {
            match("case"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CASE_KW"

    // $ANTLR start "RULE_DOT_KW"
    public final void mRULE_DOT_KW() throws RecognitionException {
        try {
            int _type = RULE_DOT_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11622:13: ( '.' )
            // InternalScilab.g:11622:15: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOT_KW"

    // $ANTLR start "RULE_PRAGMA_KW"
    public final void mRULE_PRAGMA_KW() throws RecognitionException {
        try {
            int _type = RULE_PRAGMA_KW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11624:16: ( '/#pragma_key' )
            // InternalScilab.g:11624:18: '/#pragma_key'
            {
            match("/#pragma_key"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PRAGMA_KW"

    // $ANTLR start "RULE_PRAGMA_ENTRY"
    public final void mRULE_PRAGMA_ENTRY() throws RecognitionException {
        try {
            int _type = RULE_PRAGMA_ENTRY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11626:19: ( '/#pragma_entry' (~ ( ( '\\r' | '\\n' ) ) )* RULE_CRLF )
            // InternalScilab.g:11626:21: '/#pragma_entry' (~ ( ( '\\r' | '\\n' ) ) )* RULE_CRLF
            {
            match("/#pragma_entry"); 

            // InternalScilab.g:11626:38: (~ ( ( '\\r' | '\\n' ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\u0000' && LA5_0<='\t')||(LA5_0>='\u000B' && LA5_0<='\f')||(LA5_0>='\u000E' && LA5_0<='\uFFFF')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalScilab.g:11626:38: ~ ( ( '\\r' | '\\n' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            mRULE_CRLF(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PRAGMA_ENTRY"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11628:17: ( ( '//' | '%%' ) (~ ( ( '\\r' | '\\n' ) ) )* RULE_CRLF )
            // InternalScilab.g:11628:19: ( '//' | '%%' ) (~ ( ( '\\r' | '\\n' ) ) )* RULE_CRLF
            {
            // InternalScilab.g:11628:19: ( '//' | '%%' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='/') ) {
                alt6=1;
            }
            else if ( (LA6_0=='%') ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalScilab.g:11628:20: '//'
                    {
                    match("//"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11628:25: '%%'
                    {
                    match("%%"); 


                    }
                    break;

            }

            // InternalScilab.g:11628:31: (~ ( ( '\\r' | '\\n' ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='\u0000' && LA7_0<='\t')||(LA7_0>='\u000B' && LA7_0<='\f')||(LA7_0>='\u000E' && LA7_0<='\uFFFF')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalScilab.g:11628:31: ~ ( ( '\\r' | '\\n' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            mRULE_CRLF(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_CRLF"
    public final void mRULE_CRLF() throws RecognitionException {
        try {
            int _type = RULE_CRLF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11630:11: ( ( ( '\\r' )? '\\n' | EOF ) )
            // InternalScilab.g:11630:13: ( ( '\\r' )? '\\n' | EOF )
            {
            // InternalScilab.g:11630:13: ( ( '\\r' )? '\\n' | EOF )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='\n'||LA9_0=='\r') ) {
                alt9=1;
            }
            else {
                alt9=2;}
            switch (alt9) {
                case 1 :
                    // InternalScilab.g:11630:14: ( '\\r' )? '\\n'
                    {
                    // InternalScilab.g:11630:14: ( '\\r' )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0=='\r') ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalScilab.g:11630:14: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;
                case 2 :
                    // InternalScilab.g:11630:25: EOF
                    {
                    match(EOF); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CRLF"

    // $ANTLR start "RULE_BOOLEAN_VALUE"
    public final void mRULE_BOOLEAN_VALUE() throws RecognitionException {
        try {
            int _type = RULE_BOOLEAN_VALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11632:20: ( ( 'true' | 'false' ) )
            // InternalScilab.g:11632:22: ( 'true' | 'false' )
            {
            // InternalScilab.g:11632:22: ( 'true' | 'false' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='t') ) {
                alt10=1;
            }
            else if ( (LA10_0=='f') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalScilab.g:11632:23: 'true'
                    {
                    match("true"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11632:30: 'false'
                    {
                    match("false"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BOOLEAN_VALUE"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11634:9: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( '0' .. '9' | 'A' .. 'Z' | 'a' .. 'z' | '_' )* )
            // InternalScilab.g:11634:11: ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( '0' .. '9' | 'A' .. 'Z' | 'a' .. 'z' | '_' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalScilab.g:11634:35: ( '0' .. '9' | 'A' .. 'Z' | 'a' .. 'z' | '_' )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='0' && LA11_0<='9')||(LA11_0>='A' && LA11_0<='Z')||LA11_0=='_'||(LA11_0>='a' && LA11_0<='z')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalScilab.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_DQUOTE_STRING"
    public final void mRULE_DQUOTE_STRING() throws RecognitionException {
        try {
            int _type = RULE_DQUOTE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11636:20: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' )
            // InternalScilab.g:11636:22: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
            {
            match('\"'); 
            // InternalScilab.g:11636:26: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
            loop12:
            do {
                int alt12=3;
                int LA12_0 = input.LA(1);

                if ( (LA12_0=='\\') ) {
                    alt12=1;
                }
                else if ( ((LA12_0>='\u0000' && LA12_0<='!')||(LA12_0>='#' && LA12_0<='[')||(LA12_0>=']' && LA12_0<='\uFFFF')) ) {
                    alt12=2;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalScilab.g:11636:27: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' )
            	    {
            	    match('\\'); 
            	    if ( input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;
            	case 2 :
            	    // InternalScilab.g:11636:68: ~ ( ( '\\\\' | '\"' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DQUOTE_STRING"

    // $ANTLR start "RULE_SQUOTE_STRING"
    public final void mRULE_SQUOTE_STRING() throws RecognitionException {
        try {
            int _type = RULE_SQUOTE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11638:20: ( '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            // InternalScilab.g:11638:22: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
            {
            match('\''); 
            // InternalScilab.g:11638:27: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
            loop13:
            do {
                int alt13=3;
                int LA13_0 = input.LA(1);

                if ( (LA13_0=='\\') ) {
                    alt13=1;
                }
                else if ( ((LA13_0>='\u0000' && LA13_0<='&')||(LA13_0>='(' && LA13_0<='[')||(LA13_0>=']' && LA13_0<='\uFFFF')) ) {
                    alt13=2;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalScilab.g:11638:28: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\\'' | '\\\\' )
            	    {
            	    match('\\'); 
            	    if ( input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;
            	case 2 :
            	    // InternalScilab.g:11638:69: ~ ( ( '\\\\' | '\\'' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SQUOTE_STRING"

    // $ANTLR start "RULE_ELLIPSIS"
    public final void mRULE_ELLIPSIS() throws RecognitionException {
        try {
            int _type = RULE_ELLIPSIS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11640:15: ( '...' ( RULE_WS )* ( RULE_SL_COMMENT | RULE_CRLF ) )
            // InternalScilab.g:11640:17: '...' ( RULE_WS )* ( RULE_SL_COMMENT | RULE_CRLF )
            {
            match("..."); 

            // InternalScilab.g:11640:23: ( RULE_WS )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0=='\t'||LA14_0==' ') ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalScilab.g:11640:23: RULE_WS
            	    {
            	    mRULE_WS(); 

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            // InternalScilab.g:11640:32: ( RULE_SL_COMMENT | RULE_CRLF )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='%'||LA15_0=='/') ) {
                alt15=1;
            }
            else {
                alt15=2;}
            switch (alt15) {
                case 1 :
                    // InternalScilab.g:11640:33: RULE_SL_COMMENT
                    {
                    mRULE_SL_COMMENT(); 

                    }
                    break;
                case 2 :
                    // InternalScilab.g:11640:49: RULE_CRLF
                    {
                    mRULE_CRLF(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ELLIPSIS"

    // $ANTLR start "RULE_FLOATNUMBER"
    public final void mRULE_FLOATNUMBER() throws RecognitionException {
        try {
            int _type = RULE_FLOATNUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11642:18: ( ( '0' .. '9' )* RULE_DOT_KW ( ( '0' .. '9' )+ ( ( 'e+' | 'e-' ) ( '0' .. '9' )+ )? )? )
            // InternalScilab.g:11642:20: ( '0' .. '9' )* RULE_DOT_KW ( ( '0' .. '9' )+ ( ( 'e+' | 'e-' ) ( '0' .. '9' )+ )? )?
            {
            // InternalScilab.g:11642:20: ( '0' .. '9' )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>='0' && LA16_0<='9')) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalScilab.g:11642:21: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            mRULE_DOT_KW(); 
            // InternalScilab.g:11642:44: ( ( '0' .. '9' )+ ( ( 'e+' | 'e-' ) ( '0' .. '9' )+ )? )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>='0' && LA21_0<='9')) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalScilab.g:11642:45: ( '0' .. '9' )+ ( ( 'e+' | 'e-' ) ( '0' .. '9' )+ )?
                    {
                    // InternalScilab.g:11642:45: ( '0' .. '9' )+
                    int cnt17=0;
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( ((LA17_0>='0' && LA17_0<='9')) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalScilab.g:11642:46: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt17 >= 1 ) break loop17;
                                EarlyExitException eee =
                                    new EarlyExitException(17, input);
                                throw eee;
                        }
                        cnt17++;
                    } while (true);

                    // InternalScilab.g:11642:57: ( ( 'e+' | 'e-' ) ( '0' .. '9' )+ )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0=='e') ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalScilab.g:11642:58: ( 'e+' | 'e-' ) ( '0' .. '9' )+
                            {
                            // InternalScilab.g:11642:58: ( 'e+' | 'e-' )
                            int alt18=2;
                            int LA18_0 = input.LA(1);

                            if ( (LA18_0=='e') ) {
                                int LA18_1 = input.LA(2);

                                if ( (LA18_1=='+') ) {
                                    alt18=1;
                                }
                                else if ( (LA18_1=='-') ) {
                                    alt18=2;
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 18, 1, input);

                                    throw nvae;
                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 18, 0, input);

                                throw nvae;
                            }
                            switch (alt18) {
                                case 1 :
                                    // InternalScilab.g:11642:59: 'e+'
                                    {
                                    match("e+"); 


                                    }
                                    break;
                                case 2 :
                                    // InternalScilab.g:11642:64: 'e-'
                                    {
                                    match("e-"); 


                                    }
                                    break;

                            }

                            // InternalScilab.g:11642:70: ( '0' .. '9' )+
                            int cnt19=0;
                            loop19:
                            do {
                                int alt19=2;
                                int LA19_0 = input.LA(1);

                                if ( ((LA19_0>='0' && LA19_0<='9')) ) {
                                    alt19=1;
                                }


                                switch (alt19) {
                            	case 1 :
                            	    // InternalScilab.g:11642:71: '0' .. '9'
                            	    {
                            	    matchRange('0','9'); 

                            	    }
                            	    break;

                            	default :
                            	    if ( cnt19 >= 1 ) break loop19;
                                        EarlyExitException eee =
                                            new EarlyExitException(19, input);
                                        throw eee;
                                }
                                cnt19++;
                            } while (true);


                            }
                            break;

                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOATNUMBER"

    // $ANTLR start "RULE_LONGINT"
    public final void mRULE_LONGINT() throws RecognitionException {
        try {
            int _type = RULE_LONGINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11644:14: ( ( '0' .. '9' )+ )
            // InternalScilab.g:11644:16: ( '0' .. '9' )+
            {
            // InternalScilab.g:11644:16: ( '0' .. '9' )+
            int cnt22=0;
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>='0' && LA22_0<='9')) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalScilab.g:11644:17: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt22 >= 1 ) break loop22;
                        EarlyExitException eee =
                            new EarlyExitException(22, input);
                        throw eee;
                }
                cnt22++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LONGINT"

    // $ANTLR start "RULE_IMAGINARY_FLOATNUMBER"
    public final void mRULE_IMAGINARY_FLOATNUMBER() throws RecognitionException {
        try {
            int _type = RULE_IMAGINARY_FLOATNUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11646:28: ( ( ( '0' .. '9' )* RULE_DOT_KW )? ( '0' .. '9' )+ ( 'i' | 'j' ) )
            // InternalScilab.g:11646:30: ( ( '0' .. '9' )* RULE_DOT_KW )? ( '0' .. '9' )+ ( 'i' | 'j' )
            {
            // InternalScilab.g:11646:30: ( ( '0' .. '9' )* RULE_DOT_KW )?
            int alt24=2;
            alt24 = dfa24.predict(input);
            switch (alt24) {
                case 1 :
                    // InternalScilab.g:11646:31: ( '0' .. '9' )* RULE_DOT_KW
                    {
                    // InternalScilab.g:11646:31: ( '0' .. '9' )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( ((LA23_0>='0' && LA23_0<='9')) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // InternalScilab.g:11646:32: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    mRULE_DOT_KW(); 

                    }
                    break;

            }

            // InternalScilab.g:11646:57: ( '0' .. '9' )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>='0' && LA25_0<='9')) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalScilab.g:11646:58: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);

            if ( (input.LA(1)>='i' && input.LA(1)<='j') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IMAGINARY_FLOATNUMBER"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11648:9: ( ( ' ' | '\\t' )+ )
            // InternalScilab.g:11648:11: ( ' ' | '\\t' )+
            {
            // InternalScilab.g:11648:11: ( ' ' | '\\t' )+
            int cnt26=0;
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0=='\t'||LA26_0==' ') ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalScilab.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt26 >= 1 ) break loop26;
                        EarlyExitException eee =
                            new EarlyExitException(26, input);
                        throw eee;
                }
                cnt26++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_RESERVED"
    public final void mRULE_RESERVED() throws RecognitionException {
        try {
            int _type = RULE_RESERVED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11650:15: ( ( 'SCI' | 'SCIHOME' | 'TMPDIR' | 'home' | '%e' | '%eps' | '%f' | '%F' | '%i' | '%inf' | '%nan' | '%pi' | '%s' | '%t' | '%T' | '%z' ) )
            // InternalScilab.g:11650:17: ( 'SCI' | 'SCIHOME' | 'TMPDIR' | 'home' | '%e' | '%eps' | '%f' | '%F' | '%i' | '%inf' | '%nan' | '%pi' | '%s' | '%t' | '%T' | '%z' )
            {
            // InternalScilab.g:11650:17: ( 'SCI' | 'SCIHOME' | 'TMPDIR' | 'home' | '%e' | '%eps' | '%f' | '%F' | '%i' | '%inf' | '%nan' | '%pi' | '%s' | '%t' | '%T' | '%z' )
            int alt27=16;
            alt27 = dfa27.predict(input);
            switch (alt27) {
                case 1 :
                    // InternalScilab.g:11650:18: 'SCI'
                    {
                    match("SCI"); 


                    }
                    break;
                case 2 :
                    // InternalScilab.g:11650:24: 'SCIHOME'
                    {
                    match("SCIHOME"); 


                    }
                    break;
                case 3 :
                    // InternalScilab.g:11650:34: 'TMPDIR'
                    {
                    match("TMPDIR"); 


                    }
                    break;
                case 4 :
                    // InternalScilab.g:11650:43: 'home'
                    {
                    match("home"); 


                    }
                    break;
                case 5 :
                    // InternalScilab.g:11650:50: '%e'
                    {
                    match("%e"); 


                    }
                    break;
                case 6 :
                    // InternalScilab.g:11650:55: '%eps'
                    {
                    match("%eps"); 


                    }
                    break;
                case 7 :
                    // InternalScilab.g:11650:62: '%f'
                    {
                    match("%f"); 


                    }
                    break;
                case 8 :
                    // InternalScilab.g:11650:67: '%F'
                    {
                    match("%F"); 


                    }
                    break;
                case 9 :
                    // InternalScilab.g:11650:72: '%i'
                    {
                    match("%i"); 


                    }
                    break;
                case 10 :
                    // InternalScilab.g:11650:77: '%inf'
                    {
                    match("%inf"); 


                    }
                    break;
                case 11 :
                    // InternalScilab.g:11650:84: '%nan'
                    {
                    match("%nan"); 


                    }
                    break;
                case 12 :
                    // InternalScilab.g:11650:91: '%pi'
                    {
                    match("%pi"); 


                    }
                    break;
                case 13 :
                    // InternalScilab.g:11650:97: '%s'
                    {
                    match("%s"); 


                    }
                    break;
                case 14 :
                    // InternalScilab.g:11650:102: '%t'
                    {
                    match("%t"); 


                    }
                    break;
                case 15 :
                    // InternalScilab.g:11650:107: '%T'
                    {
                    match("%T"); 


                    }
                    break;
                case 16 :
                    // InternalScilab.g:11650:112: '%z'
                    {
                    match("%z"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RESERVED"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScilab.g:11652:16: ( . )
            // InternalScilab.g:11652:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalScilab.g:1:8: ( T__70 | T__71 | RULE_ASSIGN_KW | RULE_LOR_KW | RULE_LAND_KW | RULE_OR_KW | RULE_AND_KW | RULE_LT_KW | RULE_GT_KW | RULE_LE_KW | RULE_GE_KW | RULE_EQ_KW | RULE_NEQ_KW | RULE_NOT_KW | RULE_AT_KW | RULE_COLON_KW | RULE_ARITH_KW | RULE_ADDSUB_KW | RULE_BINARY_ADDSUB_KW | RULE_UNARY_ADDSUB_KW | RULE_TRANSPOSE_KW | RULE_PREINC_KW | RULE_PREDEC_KW | RULE_POSTINC_KW | RULE_POSTDEC_KW | RULE_CONJUGATE_KW | RULE_LPAR | RULE_RPAR | RULE_LBRACKET | RULE_RBRACKET | RULE_SEMICOLON_KW | RULE_COMA_KW | RULE_RETURN_KW | RULE_FUNCTION_KW | RULE_ENDFUNCTION_KW | RULE_ENDFOR_KW | RULE_ENDIF_KW | RULE_ENDWHILE_KW | RULE_WHILE_KW | RULE_ELSE_KW | RULE_THEN_KW | RULE_ELSEIF_KW | RULE_PARFOR_KW | RULE_FOR_KW | RULE_IF_KW | RULE_END_KW | RULE_LASTINDEX_KW | RULE_BREAK_KW | RULE_CONTINUE_KW | RULE_SWITCH_KW | RULE_OTHERWISE_KW | RULE_CASE_KW | RULE_DOT_KW | RULE_PRAGMA_KW | RULE_PRAGMA_ENTRY | RULE_SL_COMMENT | RULE_CRLF | RULE_BOOLEAN_VALUE | RULE_ID | RULE_DQUOTE_STRING | RULE_SQUOTE_STRING | RULE_ELLIPSIS | RULE_FLOATNUMBER | RULE_LONGINT | RULE_IMAGINARY_FLOATNUMBER | RULE_WS | RULE_RESERVED | RULE_ANY_OTHER )
        int alt28=68;
        alt28 = dfa28.predict(input);
        switch (alt28) {
            case 1 :
                // InternalScilab.g:1:10: T__70
                {
                mT__70(); 

                }
                break;
            case 2 :
                // InternalScilab.g:1:16: T__71
                {
                mT__71(); 

                }
                break;
            case 3 :
                // InternalScilab.g:1:22: RULE_ASSIGN_KW
                {
                mRULE_ASSIGN_KW(); 

                }
                break;
            case 4 :
                // InternalScilab.g:1:37: RULE_LOR_KW
                {
                mRULE_LOR_KW(); 

                }
                break;
            case 5 :
                // InternalScilab.g:1:49: RULE_LAND_KW
                {
                mRULE_LAND_KW(); 

                }
                break;
            case 6 :
                // InternalScilab.g:1:62: RULE_OR_KW
                {
                mRULE_OR_KW(); 

                }
                break;
            case 7 :
                // InternalScilab.g:1:73: RULE_AND_KW
                {
                mRULE_AND_KW(); 

                }
                break;
            case 8 :
                // InternalScilab.g:1:85: RULE_LT_KW
                {
                mRULE_LT_KW(); 

                }
                break;
            case 9 :
                // InternalScilab.g:1:96: RULE_GT_KW
                {
                mRULE_GT_KW(); 

                }
                break;
            case 10 :
                // InternalScilab.g:1:107: RULE_LE_KW
                {
                mRULE_LE_KW(); 

                }
                break;
            case 11 :
                // InternalScilab.g:1:118: RULE_GE_KW
                {
                mRULE_GE_KW(); 

                }
                break;
            case 12 :
                // InternalScilab.g:1:129: RULE_EQ_KW
                {
                mRULE_EQ_KW(); 

                }
                break;
            case 13 :
                // InternalScilab.g:1:140: RULE_NEQ_KW
                {
                mRULE_NEQ_KW(); 

                }
                break;
            case 14 :
                // InternalScilab.g:1:152: RULE_NOT_KW
                {
                mRULE_NOT_KW(); 

                }
                break;
            case 15 :
                // InternalScilab.g:1:164: RULE_AT_KW
                {
                mRULE_AT_KW(); 

                }
                break;
            case 16 :
                // InternalScilab.g:1:175: RULE_COLON_KW
                {
                mRULE_COLON_KW(); 

                }
                break;
            case 17 :
                // InternalScilab.g:1:189: RULE_ARITH_KW
                {
                mRULE_ARITH_KW(); 

                }
                break;
            case 18 :
                // InternalScilab.g:1:203: RULE_ADDSUB_KW
                {
                mRULE_ADDSUB_KW(); 

                }
                break;
            case 19 :
                // InternalScilab.g:1:218: RULE_BINARY_ADDSUB_KW
                {
                mRULE_BINARY_ADDSUB_KW(); 

                }
                break;
            case 20 :
                // InternalScilab.g:1:240: RULE_UNARY_ADDSUB_KW
                {
                mRULE_UNARY_ADDSUB_KW(); 

                }
                break;
            case 21 :
                // InternalScilab.g:1:261: RULE_TRANSPOSE_KW
                {
                mRULE_TRANSPOSE_KW(); 

                }
                break;
            case 22 :
                // InternalScilab.g:1:279: RULE_PREINC_KW
                {
                mRULE_PREINC_KW(); 

                }
                break;
            case 23 :
                // InternalScilab.g:1:294: RULE_PREDEC_KW
                {
                mRULE_PREDEC_KW(); 

                }
                break;
            case 24 :
                // InternalScilab.g:1:309: RULE_POSTINC_KW
                {
                mRULE_POSTINC_KW(); 

                }
                break;
            case 25 :
                // InternalScilab.g:1:325: RULE_POSTDEC_KW
                {
                mRULE_POSTDEC_KW(); 

                }
                break;
            case 26 :
                // InternalScilab.g:1:341: RULE_CONJUGATE_KW
                {
                mRULE_CONJUGATE_KW(); 

                }
                break;
            case 27 :
                // InternalScilab.g:1:359: RULE_LPAR
                {
                mRULE_LPAR(); 

                }
                break;
            case 28 :
                // InternalScilab.g:1:369: RULE_RPAR
                {
                mRULE_RPAR(); 

                }
                break;
            case 29 :
                // InternalScilab.g:1:379: RULE_LBRACKET
                {
                mRULE_LBRACKET(); 

                }
                break;
            case 30 :
                // InternalScilab.g:1:393: RULE_RBRACKET
                {
                mRULE_RBRACKET(); 

                }
                break;
            case 31 :
                // InternalScilab.g:1:407: RULE_SEMICOLON_KW
                {
                mRULE_SEMICOLON_KW(); 

                }
                break;
            case 32 :
                // InternalScilab.g:1:425: RULE_COMA_KW
                {
                mRULE_COMA_KW(); 

                }
                break;
            case 33 :
                // InternalScilab.g:1:438: RULE_RETURN_KW
                {
                mRULE_RETURN_KW(); 

                }
                break;
            case 34 :
                // InternalScilab.g:1:453: RULE_FUNCTION_KW
                {
                mRULE_FUNCTION_KW(); 

                }
                break;
            case 35 :
                // InternalScilab.g:1:470: RULE_ENDFUNCTION_KW
                {
                mRULE_ENDFUNCTION_KW(); 

                }
                break;
            case 36 :
                // InternalScilab.g:1:490: RULE_ENDFOR_KW
                {
                mRULE_ENDFOR_KW(); 

                }
                break;
            case 37 :
                // InternalScilab.g:1:505: RULE_ENDIF_KW
                {
                mRULE_ENDIF_KW(); 

                }
                break;
            case 38 :
                // InternalScilab.g:1:519: RULE_ENDWHILE_KW
                {
                mRULE_ENDWHILE_KW(); 

                }
                break;
            case 39 :
                // InternalScilab.g:1:536: RULE_WHILE_KW
                {
                mRULE_WHILE_KW(); 

                }
                break;
            case 40 :
                // InternalScilab.g:1:550: RULE_ELSE_KW
                {
                mRULE_ELSE_KW(); 

                }
                break;
            case 41 :
                // InternalScilab.g:1:563: RULE_THEN_KW
                {
                mRULE_THEN_KW(); 

                }
                break;
            case 42 :
                // InternalScilab.g:1:576: RULE_ELSEIF_KW
                {
                mRULE_ELSEIF_KW(); 

                }
                break;
            case 43 :
                // InternalScilab.g:1:591: RULE_PARFOR_KW
                {
                mRULE_PARFOR_KW(); 

                }
                break;
            case 44 :
                // InternalScilab.g:1:606: RULE_FOR_KW
                {
                mRULE_FOR_KW(); 

                }
                break;
            case 45 :
                // InternalScilab.g:1:618: RULE_IF_KW
                {
                mRULE_IF_KW(); 

                }
                break;
            case 46 :
                // InternalScilab.g:1:629: RULE_END_KW
                {
                mRULE_END_KW(); 

                }
                break;
            case 47 :
                // InternalScilab.g:1:641: RULE_LASTINDEX_KW
                {
                mRULE_LASTINDEX_KW(); 

                }
                break;
            case 48 :
                // InternalScilab.g:1:659: RULE_BREAK_KW
                {
                mRULE_BREAK_KW(); 

                }
                break;
            case 49 :
                // InternalScilab.g:1:673: RULE_CONTINUE_KW
                {
                mRULE_CONTINUE_KW(); 

                }
                break;
            case 50 :
                // InternalScilab.g:1:690: RULE_SWITCH_KW
                {
                mRULE_SWITCH_KW(); 

                }
                break;
            case 51 :
                // InternalScilab.g:1:705: RULE_OTHERWISE_KW
                {
                mRULE_OTHERWISE_KW(); 

                }
                break;
            case 52 :
                // InternalScilab.g:1:723: RULE_CASE_KW
                {
                mRULE_CASE_KW(); 

                }
                break;
            case 53 :
                // InternalScilab.g:1:736: RULE_DOT_KW
                {
                mRULE_DOT_KW(); 

                }
                break;
            case 54 :
                // InternalScilab.g:1:748: RULE_PRAGMA_KW
                {
                mRULE_PRAGMA_KW(); 

                }
                break;
            case 55 :
                // InternalScilab.g:1:763: RULE_PRAGMA_ENTRY
                {
                mRULE_PRAGMA_ENTRY(); 

                }
                break;
            case 56 :
                // InternalScilab.g:1:781: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 57 :
                // InternalScilab.g:1:797: RULE_CRLF
                {
                mRULE_CRLF(); 

                }
                break;
            case 58 :
                // InternalScilab.g:1:807: RULE_BOOLEAN_VALUE
                {
                mRULE_BOOLEAN_VALUE(); 

                }
                break;
            case 59 :
                // InternalScilab.g:1:826: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 60 :
                // InternalScilab.g:1:834: RULE_DQUOTE_STRING
                {
                mRULE_DQUOTE_STRING(); 

                }
                break;
            case 61 :
                // InternalScilab.g:1:853: RULE_SQUOTE_STRING
                {
                mRULE_SQUOTE_STRING(); 

                }
                break;
            case 62 :
                // InternalScilab.g:1:872: RULE_ELLIPSIS
                {
                mRULE_ELLIPSIS(); 

                }
                break;
            case 63 :
                // InternalScilab.g:1:886: RULE_FLOATNUMBER
                {
                mRULE_FLOATNUMBER(); 

                }
                break;
            case 64 :
                // InternalScilab.g:1:903: RULE_LONGINT
                {
                mRULE_LONGINT(); 

                }
                break;
            case 65 :
                // InternalScilab.g:1:916: RULE_IMAGINARY_FLOATNUMBER
                {
                mRULE_IMAGINARY_FLOATNUMBER(); 

                }
                break;
            case 66 :
                // InternalScilab.g:1:943: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 67 :
                // InternalScilab.g:1:951: RULE_RESERVED
                {
                mRULE_RESERVED(); 

                }
                break;
            case 68 :
                // InternalScilab.g:1:965: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA1 dfa1 = new DFA1(this);
    protected DFA24 dfa24 = new DFA24(this);
    protected DFA27 dfa27 = new DFA27(this);
    protected DFA28 dfa28 = new DFA28(this);
    static final String DFA1_eotS =
        "\1\uffff\1\6\1\10\6\uffff\1\16\5\uffff";
    static final String DFA1_eofS =
        "\17\uffff";
    static final String DFA1_minS =
        "\2\52\1\57\1\52\5\uffff\1\52\5\uffff";
    static final String DFA1_maxS =
        "\1\134\1\52\1\57\1\136\5\uffff\1\52\5\uffff";
    static final String DFA1_acceptS =
        "\4\uffff\1\6\1\10\1\1\1\3\1\2\1\uffff\1\5\1\7\1\12\1\11\1\4";
    static final String DFA1_specialS =
        "\17\uffff}>";
    static final String[] DFA1_transitionS = {
            "\1\1\3\uffff\1\3\1\2\54\uffff\1\4",
            "\1\5",
            "\1\7",
            "\1\11\4\uffff\1\12\54\uffff\1\13\1\uffff\1\14",
            "",
            "",
            "",
            "",
            "",
            "\1\15",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }
        public String getDescription() {
            return "11550:17: ( '*' | '/' | '//' | '.*' | './' | '\\\\' | '.\\\\' | '**' | '.**' | '.^' )";
        }
    }
    static final String DFA24_eotS =
        "\4\uffff";
    static final String DFA24_eofS =
        "\4\uffff";
    static final String DFA24_minS =
        "\2\56\2\uffff";
    static final String DFA24_maxS =
        "\1\71\1\152\2\uffff";
    static final String DFA24_acceptS =
        "\2\uffff\1\1\1\2";
    static final String DFA24_specialS =
        "\4\uffff}>";
    static final String[] DFA24_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\2\1\uffff\12\1\57\uffff\2\3",
            "",
            ""
    };

    static final short[] DFA24_eot = DFA.unpackEncodedString(DFA24_eotS);
    static final short[] DFA24_eof = DFA.unpackEncodedString(DFA24_eofS);
    static final char[] DFA24_min = DFA.unpackEncodedStringToUnsignedChars(DFA24_minS);
    static final char[] DFA24_max = DFA.unpackEncodedStringToUnsignedChars(DFA24_maxS);
    static final short[] DFA24_accept = DFA.unpackEncodedString(DFA24_acceptS);
    static final short[] DFA24_special = DFA.unpackEncodedString(DFA24_specialS);
    static final short[][] DFA24_transition;

    static {
        int numStates = DFA24_transitionS.length;
        DFA24_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA24_transition[i] = DFA.unpackEncodedString(DFA24_transitionS[i]);
        }
    }

    class DFA24 extends DFA {

        public DFA24(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 24;
            this.eot = DFA24_eot;
            this.eof = DFA24_eof;
            this.min = DFA24_min;
            this.max = DFA24_max;
            this.accept = DFA24_accept;
            this.special = DFA24_special;
            this.transition = DFA24_transition;
        }
        public String getDescription() {
            return "11646:30: ( ( '0' .. '9' )* RULE_DOT_KW )?";
        }
    }
    static final String DFA27_eotS =
        "\6\uffff\1\22\2\uffff\1\24\6\uffff\1\26\6\uffff";
    static final String DFA27_eofS =
        "\27\uffff";
    static final String DFA27_minS =
        "\1\45\1\103\2\uffff\1\106\1\111\1\160\2\uffff\1\156\6\uffff\1\110\6\uffff";
    static final String DFA27_maxS =
        "\1\150\1\103\2\uffff\1\172\1\111\1\160\2\uffff\1\156\6\uffff\1\110\6\uffff";
    static final String DFA27_acceptS =
        "\2\uffff\1\3\1\4\3\uffff\1\7\1\10\1\uffff\1\13\1\14\1\15\1\16\1\17\1\20\1\uffff\1\6\1\5\1\12\1\11\1\2\1\1";
    static final String DFA27_specialS =
        "\27\uffff}>";
    static final String[] DFA27_transitionS = {
            "\1\4\55\uffff\1\1\1\2\23\uffff\1\3",
            "\1\5",
            "",
            "",
            "\1\10\15\uffff\1\16\20\uffff\1\6\1\7\2\uffff\1\11\4\uffff\1\12\1\uffff\1\13\2\uffff\1\14\1\15\5\uffff\1\17",
            "\1\20",
            "\1\21",
            "",
            "",
            "\1\23",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\25",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA27_eot = DFA.unpackEncodedString(DFA27_eotS);
    static final short[] DFA27_eof = DFA.unpackEncodedString(DFA27_eofS);
    static final char[] DFA27_min = DFA.unpackEncodedStringToUnsignedChars(DFA27_minS);
    static final char[] DFA27_max = DFA.unpackEncodedStringToUnsignedChars(DFA27_maxS);
    static final short[] DFA27_accept = DFA.unpackEncodedString(DFA27_acceptS);
    static final short[] DFA27_special = DFA.unpackEncodedString(DFA27_specialS);
    static final short[][] DFA27_transition;

    static {
        int numStates = DFA27_transitionS.length;
        DFA27_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA27_transition[i] = DFA.unpackEncodedString(DFA27_transitionS[i]);
        }
    }

    class DFA27 extends DFA {

        public DFA27(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 27;
            this.eot = DFA27_eot;
            this.eof = DFA27_eof;
            this.min = DFA27_min;
            this.max = DFA27_max;
            this.accept = DFA27_accept;
            this.special = DFA27_special;
            this.transition = DFA27_transition;
        }
        public String getDescription() {
            return "11650:17: ( 'SCI' | 'SCIHOME' | 'TMPDIR' | 'home' | '%e' | '%eps' | '%f' | '%F' | '%i' | '%inf' | '%nan' | '%pi' | '%s' | '%t' | '%T' | '%z' )";
        }
    }
    static final String DFA28_eotS =
        "\1\44\2\57\1\64\1\66\1\70\1\72\1\74\1\100\4\uffff\1\106\1\uffff\1\55\6\uffff\6\57\1\uffff\4\57\2\55\2\uffff\1\57\2\55\1\147\1\uffff\2\57\2\uffff\1\57\1\uffff\3\57\13\uffff\1\100\10\uffff\1\163\12\uffff\11\57\1\175\1\uffff\5\57\2\uffff\1\57\3\uffff\1\147\1\uffff\1\163\1\uffff\6\57\2\uffff\2\57\1\u008f\1\57\1\u0094\4\57\1\uffff\12\57\2\uffff\1\57\1\uffff\3\57\1\uffff\4\57\1\uffff\1\u00af\1\57\1\u00b1\1\u00b2\2\57\1\u00b5\4\57\1\uffff\2\57\3\uffff\3\57\1\u00b2\2\57\1\u00c2\2\57\1\uffff\1\u00c5\2\uffff\1\u00c6\1\57\1\uffff\4\57\1\u00cc\1\57\1\uffff\1\u00cf\1\57\1\u00d1\1\57\1\u00d3\1\uffff\1\57\1\u00d5\2\uffff\1\57\1\u00d7\2\57\2\uffff\1\57\2\uffff\1\57\1\uffff\1\57\1\uffff\1\57\1\uffff\1\57\1\uffff\1\57\1\uffff\1\57\1\uffff\1\u00e3\1\57\1\u00e5\1\u00e6\2\57\2\uffff\1\57\2\uffff\1\u00ec\1\u00ed\2\uffff\1\57\2\uffff\1\u00ef\1\uffff";
    static final String DFA28_eofS =
        "\u00f0\uffff";
    static final String DFA28_minS =
        "\1\0\1\154\1\145\1\75\1\174\1\46\2\75\1\43\4\uffff\1\47\1\uffff\1\47\6\uffff\1\145\1\141\1\154\2\150\1\146\1\uffff\1\162\1\141\1\167\1\164\1\45\1\12\2\uffff\1\103\2\0\1\56\1\uffff\1\115\1\157\2\uffff\1\157\1\uffff\1\162\1\145\1\163\13\uffff\1\0\1\160\7\uffff\1\60\12\uffff\1\164\1\156\1\162\1\154\1\144\1\163\1\151\1\145\1\165\1\60\1\uffff\1\145\1\156\1\163\1\151\1\150\2\uffff\1\111\3\uffff\1\56\1\uffff\1\60\1\uffff\1\120\1\155\1\142\1\163\1\53\1\164\1\162\1\uffff\1\165\1\143\1\60\1\163\1\60\1\145\1\154\1\156\1\145\1\uffff\1\141\1\164\1\145\1\164\1\145\1\110\1\104\1\145\1\141\1\151\2\uffff\1\53\1\141\1\162\1\164\1\154\1\uffff\1\145\1\157\1\146\1\150\1\uffff\1\60\1\145\2\60\1\153\1\151\1\60\1\143\1\162\1\117\1\111\1\uffff\1\154\1\163\2\uffff\1\147\1\156\1\151\1\154\1\60\1\156\1\162\1\60\1\151\1\146\1\uffff\1\60\2\uffff\1\60\1\156\1\uffff\1\150\1\167\1\115\1\122\1\60\1\164\1\155\1\60\1\157\1\60\1\143\1\60\1\uffff\1\154\1\60\2\uffff\1\165\1\60\1\151\1\105\2\uffff\1\145\1\141\1\uffff\1\156\1\uffff\1\164\1\uffff\1\145\1\uffff\1\145\1\uffff\1\163\1\uffff\1\156\1\137\1\60\1\151\2\60\1\145\1\164\1\145\1\uffff\1\157\2\uffff\2\60\2\uffff\1\156\2\uffff\1\60\1\uffff";
    static final String DFA28_maxS =
        "\1\uffff\1\154\1\162\1\75\1\174\1\46\3\75\4\uffff\1\136\1\uffff\1\125\6\uffff\1\145\1\165\1\156\1\150\1\162\1\146\1\uffff\1\162\1\157\1\167\1\164\1\172\1\12\2\uffff\1\103\2\uffff\1\152\1\uffff\1\115\1\157\2\uffff\1\157\1\uffff\1\162\1\145\1\163\13\uffff\1\uffff\1\160\7\uffff\1\152\12\uffff\1\164\1\156\1\162\1\154\1\144\1\163\1\151\1\145\1\165\1\172\1\uffff\1\145\1\156\1\163\1\151\1\150\2\uffff\1\111\3\uffff\1\152\1\uffff\1\71\1\uffff\1\120\1\155\1\142\1\163\1\55\1\164\1\162\1\uffff\1\165\1\143\1\172\1\163\1\172\1\145\1\154\1\156\1\145\1\uffff\1\141\1\164\1\145\1\164\1\145\1\110\1\104\1\145\1\141\1\151\2\uffff\1\55\1\141\1\162\1\164\1\154\1\uffff\1\145\1\165\1\146\1\150\1\uffff\1\172\1\145\2\172\1\153\1\151\1\172\1\143\1\162\1\117\1\111\1\uffff\1\154\1\163\2\uffff\1\147\1\156\1\151\1\154\1\172\1\156\1\162\1\172\1\151\1\146\1\uffff\1\172\2\uffff\1\172\1\156\1\uffff\1\150\1\167\1\115\1\122\1\172\1\164\1\155\1\172\1\157\1\172\1\143\1\172\1\uffff\1\154\1\172\2\uffff\1\165\1\172\1\151\1\105\2\uffff\1\145\1\141\1\uffff\1\156\1\uffff\1\164\1\uffff\1\145\1\uffff\1\145\1\uffff\1\163\1\uffff\1\156\1\137\1\172\1\151\2\172\1\145\1\164\1\153\1\uffff\1\157\2\uffff\2\172\2\uffff\1\156\2\uffff\1\172\1\uffff";
    static final String DFA28_acceptS =
        "\11\uffff\1\16\1\17\1\20\1\21\1\uffff\1\21\1\uffff\1\33\1\34\1\35\1\36\1\37\1\40\6\uffff\1\57\6\uffff\2\71\4\uffff\1\102\2\uffff\1\73\1\104\1\uffff\1\73\3\uffff\1\14\1\3\1\4\1\6\1\5\1\7\1\12\1\10\1\13\1\11\1\15\2\uffff\1\21\1\16\1\17\1\20\1\32\1\76\1\65\1\uffff\1\22\1\23\1\24\1\25\1\33\1\34\1\35\1\36\1\37\1\40\12\uffff\1\57\5\uffff\1\70\1\103\1\uffff\1\74\1\75\1\100\1\uffff\1\101\1\uffff\1\102\7\uffff\1\77\11\uffff\1\55\12\uffff\1\26\1\27\5\uffff\1\54\4\uffff\1\56\13\uffff\1\73\2\uffff\1\30\1\31\12\uffff\1\50\1\uffff\1\51\1\72\2\uffff\1\64\14\uffff\1\45\2\uffff\1\47\1\60\4\uffff\1\73\1\1\2\uffff\1\41\1\uffff\1\53\1\uffff\1\44\1\uffff\1\52\1\uffff\1\62\1\uffff\1\73\11\uffff\1\42\1\uffff\1\46\1\61\2\uffff\1\66\1\67\1\uffff\1\63\1\2\1\uffff\1\43";
    static final String DFA28_specialS =
        "\1\3\45\uffff\1\2\1\0\26\uffff\1\1\u00b1\uffff}>";
    static final String[] DFA28_transitionS = {
            "\11\55\1\51\1\43\2\55\1\42\22\55\1\51\1\55\1\46\1\17\1\34\1\41\1\5\1\47\1\20\1\21\1\14\1\55\1\25\1\55\1\15\1\10\12\50\1\13\1\24\1\6\1\3\1\7\1\55\1\12\22\54\1\45\1\52\6\54\1\22\1\16\1\23\1\55\1\54\1\55\1\54\1\35\1\36\1\54\1\30\1\27\1\1\1\53\1\33\5\54\1\40\1\2\1\54\1\26\1\37\1\32\2\54\1\31\3\54\1\55\1\4\1\55\1\11\uff81\55",
            "\1\56",
            "\1\60\11\uffff\1\62\2\uffff\1\61",
            "\1\63",
            "\1\65",
            "\1\67",
            "\1\71",
            "\1\73",
            "\1\77\13\uffff\1\76\15\uffff\1\75",
            "",
            "",
            "",
            "",
            "\1\104\2\uffff\1\100\3\uffff\1\105\1\100\12\107\42\uffff\1\100\1\uffff\1\100",
            "",
            "\1\113\31\uffff\1\110\1\111\22\uffff\1\112",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\122",
            "\1\125\15\uffff\1\124\5\uffff\1\123",
            "\1\127\1\uffff\1\126",
            "\1\130",
            "\1\131\11\uffff\1\132",
            "\1\133",
            "",
            "\1\135",
            "\1\137\15\uffff\1\136",
            "\1\140",
            "\1\141",
            "\1\142\40\uffff\1\143\15\uffff\1\143\20\uffff\2\143\2\uffff\1\143\4\uffff\1\143\1\uffff\1\143\2\uffff\2\143\5\uffff\1\143",
            "\1\44",
            "",
            "",
            "\1\144",
            "\0\145",
            "\0\146",
            "\1\152\1\uffff\12\150\57\uffff\2\151",
            "",
            "\1\154",
            "\1\155",
            "",
            "",
            "\1\156",
            "",
            "\1\157",
            "\1\160",
            "\1\161",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\0\142",
            "\1\162",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\107\57\uffff\2\151",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "",
            "",
            "\1\u0083",
            "",
            "",
            "",
            "\1\152\1\uffff\12\150\57\uffff\2\151",
            "",
            "\12\107",
            "",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088\1\uffff\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "",
            "\1\u008c",
            "\1\u008d",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\1\u008e\31\57",
            "\1\u0090",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\5\57\1\u0091\2\57\1\u0092\15\57\1\u0093\3\57",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "",
            "",
            "\1\u00a3\1\uffff\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "",
            "\1\u00a9",
            "\1\u00ab\5\uffff\1\u00aa",
            "\1\u00ac",
            "\1\u00ad",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\10\57\1\u00ae\21\57",
            "\1\u00b0",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00b3",
            "\1\u00b4",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00b6",
            "\1\u00b7",
            "\1\u00b8",
            "\1\u00b9",
            "",
            "\1\u00ba",
            "\1\u00bb",
            "",
            "",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00c0",
            "\1\u00c1",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00c3",
            "\1\u00c4",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00c7",
            "",
            "\1\u00c8",
            "\1\u00c9",
            "\1\u00ca",
            "\1\u00cb",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00cd",
            "\1\u00ce",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00d0",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00d2",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "\1\u00d4",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "",
            "\1\u00d6",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00d8",
            "\1\u00d9",
            "",
            "",
            "\1\u00da",
            "\1\u00db",
            "",
            "\1\u00dc",
            "",
            "\1\u00dd",
            "",
            "\1\u00de",
            "",
            "\1\u00df",
            "",
            "\1\u00e0",
            "",
            "\1\u00e1",
            "\1\u00e2",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00e4",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\u00e7",
            "\1\u00e8",
            "\1\u00ea\5\uffff\1\u00e9",
            "",
            "\1\u00eb",
            "",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "",
            "\1\u00ee",
            "",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            ""
    };

    static final short[] DFA28_eot = DFA.unpackEncodedString(DFA28_eotS);
    static final short[] DFA28_eof = DFA.unpackEncodedString(DFA28_eofS);
    static final char[] DFA28_min = DFA.unpackEncodedStringToUnsignedChars(DFA28_minS);
    static final char[] DFA28_max = DFA.unpackEncodedStringToUnsignedChars(DFA28_maxS);
    static final short[] DFA28_accept = DFA.unpackEncodedString(DFA28_acceptS);
    static final short[] DFA28_special = DFA.unpackEncodedString(DFA28_specialS);
    static final short[][] DFA28_transition;

    static {
        int numStates = DFA28_transitionS.length;
        DFA28_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA28_transition[i] = DFA.unpackEncodedString(DFA28_transitionS[i]);
        }
    }

    class DFA28 extends DFA {

        public DFA28(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 28;
            this.eot = DFA28_eot;
            this.eof = DFA28_eof;
            this.min = DFA28_min;
            this.max = DFA28_max;
            this.accept = DFA28_accept;
            this.special = DFA28_special;
            this.transition = DFA28_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__70 | T__71 | RULE_ASSIGN_KW | RULE_LOR_KW | RULE_LAND_KW | RULE_OR_KW | RULE_AND_KW | RULE_LT_KW | RULE_GT_KW | RULE_LE_KW | RULE_GE_KW | RULE_EQ_KW | RULE_NEQ_KW | RULE_NOT_KW | RULE_AT_KW | RULE_COLON_KW | RULE_ARITH_KW | RULE_ADDSUB_KW | RULE_BINARY_ADDSUB_KW | RULE_UNARY_ADDSUB_KW | RULE_TRANSPOSE_KW | RULE_PREINC_KW | RULE_PREDEC_KW | RULE_POSTINC_KW | RULE_POSTDEC_KW | RULE_CONJUGATE_KW | RULE_LPAR | RULE_RPAR | RULE_LBRACKET | RULE_RBRACKET | RULE_SEMICOLON_KW | RULE_COMA_KW | RULE_RETURN_KW | RULE_FUNCTION_KW | RULE_ENDFUNCTION_KW | RULE_ENDFOR_KW | RULE_ENDIF_KW | RULE_ENDWHILE_KW | RULE_WHILE_KW | RULE_ELSE_KW | RULE_THEN_KW | RULE_ELSEIF_KW | RULE_PARFOR_KW | RULE_FOR_KW | RULE_IF_KW | RULE_END_KW | RULE_LASTINDEX_KW | RULE_BREAK_KW | RULE_CONTINUE_KW | RULE_SWITCH_KW | RULE_OTHERWISE_KW | RULE_CASE_KW | RULE_DOT_KW | RULE_PRAGMA_KW | RULE_PRAGMA_ENTRY | RULE_SL_COMMENT | RULE_CRLF | RULE_BOOLEAN_VALUE | RULE_ID | RULE_DQUOTE_STRING | RULE_SQUOTE_STRING | RULE_ELLIPSIS | RULE_FLOATNUMBER | RULE_LONGINT | RULE_IMAGINARY_FLOATNUMBER | RULE_WS | RULE_RESERVED | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA28_39 = input.LA(1);

                        s = -1;
                        if ( ((LA28_39>='\u0000' && LA28_39<='\uFFFF')) ) {s = 102;}

                        else s = 45;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA28_62 = input.LA(1);

                        s = -1;
                        if ( ((LA28_62>='\u0000' && LA28_62<='\uFFFF')) ) {s = 98;}

                        else s = 64;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA28_38 = input.LA(1);

                        s = -1;
                        if ( ((LA28_38>='\u0000' && LA28_38<='\uFFFF')) ) {s = 101;}

                        else s = 45;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA28_0 = input.LA(1);

                        s = -1;
                        if ( (LA28_0=='g') ) {s = 1;}

                        else if ( (LA28_0=='p') ) {s = 2;}

                        else if ( (LA28_0=='=') ) {s = 3;}

                        else if ( (LA28_0=='|') ) {s = 4;}

                        else if ( (LA28_0=='&') ) {s = 5;}

                        else if ( (LA28_0=='<') ) {s = 6;}

                        else if ( (LA28_0=='>') ) {s = 7;}

                        else if ( (LA28_0=='/') ) {s = 8;}

                        else if ( (LA28_0=='~') ) {s = 9;}

                        else if ( (LA28_0=='@') ) {s = 10;}

                        else if ( (LA28_0==':') ) {s = 11;}

                        else if ( (LA28_0=='*') ) {s = 12;}

                        else if ( (LA28_0=='.') ) {s = 13;}

                        else if ( (LA28_0=='\\') ) {s = 14;}

                        else if ( (LA28_0=='#') ) {s = 15;}

                        else if ( (LA28_0=='(') ) {s = 16;}

                        else if ( (LA28_0==')') ) {s = 17;}

                        else if ( (LA28_0=='[') ) {s = 18;}

                        else if ( (LA28_0==']') ) {s = 19;}

                        else if ( (LA28_0==';') ) {s = 20;}

                        else if ( (LA28_0==',') ) {s = 21;}

                        else if ( (LA28_0=='r') ) {s = 22;}

                        else if ( (LA28_0=='f') ) {s = 23;}

                        else if ( (LA28_0=='e') ) {s = 24;}

                        else if ( (LA28_0=='w') ) {s = 25;}

                        else if ( (LA28_0=='t') ) {s = 26;}

                        else if ( (LA28_0=='i') ) {s = 27;}

                        else if ( (LA28_0=='$') ) {s = 28;}

                        else if ( (LA28_0=='b') ) {s = 29;}

                        else if ( (LA28_0=='c') ) {s = 30;}

                        else if ( (LA28_0=='s') ) {s = 31;}

                        else if ( (LA28_0=='o') ) {s = 32;}

                        else if ( (LA28_0=='%') ) {s = 33;}

                        else if ( (LA28_0=='\r') ) {s = 34;}

                        else if ( (LA28_0=='\n') ) {s = 35;}

                        else if ( (LA28_0=='S') ) {s = 37;}

                        else if ( (LA28_0=='\"') ) {s = 38;}

                        else if ( (LA28_0=='\'') ) {s = 39;}

                        else if ( ((LA28_0>='0' && LA28_0<='9')) ) {s = 40;}

                        else if ( (LA28_0=='\t'||LA28_0==' ') ) {s = 41;}

                        else if ( (LA28_0=='T') ) {s = 42;}

                        else if ( (LA28_0=='h') ) {s = 43;}

                        else if ( ((LA28_0>='A' && LA28_0<='R')||(LA28_0>='U' && LA28_0<='Z')||LA28_0=='_'||LA28_0=='a'||LA28_0=='d'||(LA28_0>='j' && LA28_0<='n')||LA28_0=='q'||(LA28_0>='u' && LA28_0<='v')||(LA28_0>='x' && LA28_0<='z')) ) {s = 44;}

                        else if ( ((LA28_0>='\u0000' && LA28_0<='\b')||(LA28_0>='\u000B' && LA28_0<='\f')||(LA28_0>='\u000E' && LA28_0<='\u001F')||LA28_0=='!'||LA28_0=='+'||LA28_0=='-'||LA28_0=='?'||LA28_0=='^'||LA28_0=='`'||LA28_0=='{'||LA28_0=='}'||(LA28_0>='\u007F' && LA28_0<='\uFFFF')) ) {s = 45;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 28, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}