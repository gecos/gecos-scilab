package fr.irisa.cairn.gecos.ide

import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import com.google.inject.Inject
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.gecos.services.ScilabGrammarAccess
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.ide.editor.syntaxcoloring.HighlightingStyles
import fr.irisa.cairn.gecos.scilab.ScilabPackage
import gecos.blocks.ForBlock

public class ScilabSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {

    @Inject package ScilabGrammarAccess grammar

    override protected boolean highlightElement(EObject object, IHighlightedPositionAcceptor acceptor,
        CancelIndicator cancelIndicator) {
        switch (object) {
            ForBlock: {
                highlightFeature(acceptor, object, ScilabPackage.eINSTANCE.scilabForBlock_Range, HighlightingStyles.DEFAULT_ID)
                return true
            }
            default: false
        }
    }
}
