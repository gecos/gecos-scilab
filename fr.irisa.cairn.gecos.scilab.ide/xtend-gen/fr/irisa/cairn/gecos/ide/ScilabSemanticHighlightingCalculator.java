package fr.irisa.cairn.gecos.ide;

import com.google.inject.Inject;
import fr.irisa.cairn.gecos.scilab.ScilabPackage;
import fr.irisa.cairn.gecos.services.ScilabGrammarAccess;
import gecos.blocks.ForBlock;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.ide.editor.syntaxcoloring.HighlightingStyles;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.util.CancelIndicator;

@SuppressWarnings("all")
public class ScilabSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {
  @Inject
  ScilabGrammarAccess grammar;
  
  @Override
  protected boolean highlightElement(final EObject object, final IHighlightedPositionAcceptor acceptor, final CancelIndicator cancelIndicator) {
    boolean _switchResult = false;
    boolean _matched = false;
    if (object instanceof ForBlock) {
      _matched=true;
      this.highlightFeature(acceptor, object, ScilabPackage.eINSTANCE.getScilabForBlock_Range(), HighlightingStyles.DEFAULT_ID);
      return true;
    }
    if (!_matched) {
      _switchResult = false;
    }
    return _switchResult;
  }
}
