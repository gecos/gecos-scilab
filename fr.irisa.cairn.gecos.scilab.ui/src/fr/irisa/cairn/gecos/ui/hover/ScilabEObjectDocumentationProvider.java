package fr.irisa.cairn.gecos.ui.hover;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider;


import fr.irisa.cairn.gecos.model.scilab.ScilabForBlock;
import gecos.blocks.ForBlock;

public class ScilabEObjectDocumentationProvider implements
		IEObjectDocumentationProvider {

	public String getDocumentation(EObject o) {
//		if(o instanceof EObject) {
			EObject sp = (EObject)o;
			EClass spEClass = sp.eClass();
			return "EClass: " + spEClass.getName();
//		}
//		return "Default tooltip for "+o.getClass().getSimpleName();
	}
}