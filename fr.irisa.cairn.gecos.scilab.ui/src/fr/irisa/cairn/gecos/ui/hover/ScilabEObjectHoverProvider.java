package fr.irisa.cairn.gecos.ui.hover;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;


import fr.irisa.cairn.gecos.model.scilab.ScilabForBlock;
import fr.irisa.cairn.gecos.model.scilab.ScilabProcedure;

public class ScilabEObjectHoverProvider extends
		DefaultEObjectHoverProvider {

	@Override
	protected String getFirstLine(EObject o) {
		if(o instanceof ScilabForBlock) {
			return "Source Pattern";
		}
		if(o instanceof ScilabProcedure) {
			return "Target Pattern";
		}
		return null;
	}
	
	@Override
	protected boolean hasHover(EObject o) {
		if(o instanceof ScilabForBlock) {
			ScilabForBlock sp = (ScilabForBlock)o;
			return sp.eClass() != null;
		}
		return super.hasHover(o);
	}	
	
}