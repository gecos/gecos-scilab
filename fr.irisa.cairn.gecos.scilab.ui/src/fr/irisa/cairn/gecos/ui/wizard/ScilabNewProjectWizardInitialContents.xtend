/*
 * generated by Xtext 2.13.0
 */
package fr.irisa.cairn.gecos.ui.wizard


import com.google.inject.Inject
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.resource.FileExtensionProvider

class ScilabNewProjectWizardInitialContents {
	@Inject
	FileExtensionProvider fileExtensionProvider

	def generateInitialContents(IFileSystemAccess2 fsa) {
		fsa.generateFile(
			"src/test." + fileExtensionProvider.primaryFileExtension,
			'''
			a = rand(10,10);
			'''
			)
	}
}
