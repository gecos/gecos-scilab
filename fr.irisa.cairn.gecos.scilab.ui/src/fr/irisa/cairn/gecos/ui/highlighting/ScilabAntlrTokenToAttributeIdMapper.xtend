package fr.irisa.cairn.gecos.ui.highlighting

import java.util.Arrays
import java.util.List
import org.eclipse.emf.ecore.EPackage
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper

class ScilabAntlrTokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {
	
	List<String> specials = #[
		"RULE_PRAGMA_KW"
		]
	
	List<String> keyWords = #[
		"RULE_FOR_KW",
		"RULE_PARFOR_KW",
		"RULE_THEN_KW",
		"RULE_IF_KW", 
		"RULE_FUNCTION_KW", 
		"RULE_ENDFUNCTION_KW", 
		"RULE_ELSE_KW", 
		"RULE_SWITCH_KW", 
		"RULE_ELSE_KW",
		"RULE_OTHERWISE_KW", 
		"RULE_CASE_KW",
		"RULE_WHILE_KW", 
		"RULE_ENDWHILE_KW",
		"RULE_ELSE_KW", 
		"RULE_ENDFOR_KW", 
		"RULE_RETURN_KW", 
		"RULE_BREAK_KW", 
		"RULE_CONTINUE_KW", 
		"RULE_AT_KW", 
		"RULE_END_KW", 
		"RULE_ELSE_KW"
	]

	List<String> litteral = #[
		"RULE_LONGINT","RULE_FLOATNUMBER","RULE_IMAGINARY_FLOATNUMBER","RULE_DQUOTE_STRING","RULE_SQUOTE_STRING","RULE_BOOLEAN_VALUE"
	]


	override protected String calculateId(String tokenName, int tokenType) {
		if (keyWords.contains(tokenName)) {
			return ScilabHighlightingConfiguration.KEY_BOLD
		}
		if (litteral.contains(tokenName)) {
			return ScilabHighlightingConfiguration.LITTERAL
		}
		if (specials.contains(tokenName)) {
			return ScilabHighlightingConfiguration.BUILTIN
		}
		return super.calculateId(tokenName, tokenType)
	}
}
