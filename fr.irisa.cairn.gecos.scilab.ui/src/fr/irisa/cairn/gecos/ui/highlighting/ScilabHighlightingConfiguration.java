package fr.irisa.cairn.gecos.ui.highlighting;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class ScilabHighlightingConfiguration extends DefaultHighlightingConfiguration
		implements IHighlightingConfiguration {

	public static final String KEY_BOLD = "scilab keyword";
	public static final String BUILTIN = "scilab builtin";
	public static final String BASE_LIB = "scilab base library";
	public static final String LITTERAL = "scilab litteral";
	public static final String HOTREGION = "scilab hotregion";

	@Override
	public void configure(IHighlightingConfigurationAcceptor acceptor) {
		super.configure(acceptor);
		acceptor.acceptDefaultHighlighting(KEY_BOLD, "scilab keyword", scilabKeyword());
		acceptor.acceptDefaultHighlighting(BUILTIN, "scilab builtin function", scilabBuiltin());
		acceptor.acceptDefaultHighlighting(LITTERAL, "scilab litteral", scilabLitteral());
		acceptor.acceptDefaultHighlighting(HOTREGION, "scilab litteral", scilabHotRegion());
		//acceptor.acceptDefaultHighlighting(BUILTIN, "scilab builtin function", scilabBuiltin());
	}

	public TextStyle scilabKeyword() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(31, 50, 125));
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}

	public TextStyle scilabBuiltin() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(200, 50, 50));
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}

	public TextStyle scilabLitteral() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(100, 125, 70));
		return textStyle;
	}
	
	public TextStyle scilabHotRegion() {
		TextStyle textStyle = new TextStyle();
		textStyle.setBackgroundColor(new RGB(100, 250, 100));
		return textStyle;
	}

}