package fr.irisa.cairn.gecos.ui.highlighting;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;

public class ScilabSemanticHighlightingCalculator implements
		ISemanticHighlightingCalculator {

	@Override
	public void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
			CancelIndicator cancelIndicator) {
		  INode root = resource.getParseResult().getRootNode();
		  for (INode node : root.getAsTreeIterable()) {
			  if(node.getGrammarElement() instanceof RuleCall) {
				  RuleCall r = (RuleCall)node.getGrammarElement();
				  System.out.println(r.getRule().getName());
				  if(r.getRule().getName().equals("ForStatement")) {
					acceptor.addPosition(node.getOffset(), node.getLength(),
							ScilabHighlightingConfiguration.HOTREGION);
				  }
				  if(r.getRule().getName().equals("Pragma")) {
					acceptor.addPosition(node.getOffset(), node.getLength(),
							ScilabHighlightingConfiguration.BUILTIN);
				  }
				  if(r.getRule().getName().equals("ClassPatternExp")) {
					acceptor.addPosition(node.getOffset(), node.getLength(),
							ScilabHighlightingConfiguration.KEY_BOLD);
				  }
			  }
		  }
		
	}

}