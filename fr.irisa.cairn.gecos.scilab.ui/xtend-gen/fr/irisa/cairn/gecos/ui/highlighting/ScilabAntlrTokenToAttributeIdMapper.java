package fr.irisa.cairn.gecos.ui.highlighting;

import fr.irisa.cairn.gecos.ui.highlighting.ScilabHighlightingConfiguration;
import java.util.Collections;
import java.util.List;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class ScilabAntlrTokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {
  private List<String> specials = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("RULE_PRAGMA_KW"));
  
  private List<String> keyWords = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("RULE_FOR_KW", "RULE_PARFOR_KW", "RULE_THEN_KW", "RULE_IF_KW", "RULE_FUNCTION_KW", "RULE_ENDFUNCTION_KW", "RULE_ELSE_KW", "RULE_SWITCH_KW", "RULE_ELSE_KW", "RULE_OTHERWISE_KW", "RULE_CASE_KW", "RULE_WHILE_KW", "RULE_ENDWHILE_KW", "RULE_ELSE_KW", "RULE_ENDFOR_KW", "RULE_RETURN_KW", "RULE_BREAK_KW", "RULE_CONTINUE_KW", "RULE_AT_KW", "RULE_END_KW", "RULE_ELSE_KW"));
  
  private List<String> litteral = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("RULE_LONGINT", "RULE_FLOATNUMBER", "RULE_IMAGINARY_FLOATNUMBER", "RULE_DQUOTE_STRING", "RULE_SQUOTE_STRING", "RULE_BOOLEAN_VALUE"));
  
  @Override
  protected String calculateId(final String tokenName, final int tokenType) {
    boolean _contains = this.keyWords.contains(tokenName);
    if (_contains) {
      return ScilabHighlightingConfiguration.KEY_BOLD;
    }
    boolean _contains_1 = this.litteral.contains(tokenName);
    if (_contains_1) {
      return ScilabHighlightingConfiguration.LITTERAL;
    }
    boolean _contains_2 = this.specials.contains(tokenName);
    if (_contains_2) {
      return ScilabHighlightingConfiguration.BUILTIN;
    }
    return super.calculateId(tokenName, tokenType);
  }
}
