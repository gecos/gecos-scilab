/*
 * generated by Xtext 2.12.0
 */
package fr.irisa.cairn.gecos.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator

import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration
import fr.irisa.cairn.gecos.ui.highlighting.ScilabHighlightingConfiguration
import fr.irisa.cairn.gecos.ui.highlighting.ScilabAntlrTokenToAttributeIdMapper
import org.eclipse.xtext.ui.editor.syntaxcoloring.AbstractAntlrTokenToAttributeIdMapper
import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider
import fr.irisa.cairn.gecos.ui.hover.ScilabEObjectHoverProvider
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider
import fr.irisa.cairn.gecos.ui.hover.ScilabEObjectDocumentationProvider
import fr.irisa.cairn.gecos.ui.highlighting.ScilabSemanticHighlightingCalculator
import fr.irisa.cairn.gecos.ScilabCustomizedLexer

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class ScilabUiModule extends AbstractScilabUiModule {
	
	def Class<? extends IHighlightingConfiguration> bindIHighlightingConfiguration () {
	     return ScilabHighlightingConfiguration;
	}

	
	def Class<? extends AbstractAntlrTokenToAttributeIdMapper> bindAbstractAntlrTokenToAttributeIdMapper() {
		return ScilabAntlrTokenToAttributeIdMapper;
	}
	
	def Class<? extends ISemanticHighlightingCalculator> bindISemanticHighlightingCalculator() {
		return ScilabSemanticHighlightingCalculator;
	}
	
//	def Class<? extends LazyLinker> bindLazyLinker() {
//		return ScilabLazyLinker;
//	}
	
	def Class<? extends IEObjectHoverProvider> bindIEobjectHoverProvider() {
		return ScilabEObjectHoverProvider;
	}
	
	def configureRuntimeLexer(com.google.inject.Binder binder) {
		binder.bind(org.eclipse.xtext.parser.antlr.Lexer).annotatedWith(com.google.inject.name.Names.named(org.eclipse.xtext.parser.antlr.LexerBindings.RUNTIME)).to(ScilabCustomizedLexer);
	}
	
	def Class<? extends IEObjectDocumentationProvider> bindIEobjectDocumentationProvider() {
		return ScilabEObjectDocumentationProvider;
	}	

}
